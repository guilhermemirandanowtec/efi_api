/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.config.fullPage = true;
CKEDITOR.config.extraPlugins = 'autogrow,html5audio'; 
CKEDITOR.config.allowedContent = true;

CKEDITOR.editorConfig = function( config ) {

	config.autoGrow_onStartup = true;
	config.disableNativeSpellChecker = false;

};
