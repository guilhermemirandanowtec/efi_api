﻿


var url = "http://www.youtube.com/embed/";

/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.addTemplates("default", {
  imagesPath: CKEDITOR.getUrl(
    CKEDITOR.plugins.getPath("templates") + "templates/imagens/"
  ),
  templates: [
    /*{title:'Criar Colunas',image:'template2.gif',description:'Template que define 3 colunas.',html:'<style type="text/css">#tabelacontent{width:32%;float:left;border:1px solid #000;height:500px;overflow:auto;padding:2px;margin:1px;}#tabelacms{height:450px; vertical-align:top;}</style><div id=tabelacontent><table border=0 cellpadding=0 cellspacing=0 style=width:100%;height:500px;><tbody><tr><td id=tabelacms>&nbsp;</td></tr></tbody></table></div><div id=tabelacontent><table border=0 cellpadding=0 cellspacing=0 style=width:100%;height:500px;><tbody><tr><td id=tabelacms>&nbsp;</td></tr></tbody></table></div><div id=tabelacontent><table border=0 cellpadding=0 cellspacing=0 style=width:100%;height:500px;><tbody><tr><td id=tabelacms>&nbsp;</td></tr></tbody></table></div>'},*/

    //texto
    {
      title: "Texto",
      image: "template2.gif",
      description: "Template de texto.",
      html:
        '<style>#conteudo{-moz-column-count: 2;-webkit-column-count: 2; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; font-size: 14px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align: justify;"><div id="conteudo" style=" border: 0px;" ><p>Conte&uacute;do Aqui</div></p>'
    },

    //Video
    {
      title: "Videos",
      image: "template2.gif",
      description: "Template de video.",
      html:
        '<style>#conteudo{-moz-column-count: 2;-webkit-column-count: 2; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; font-size: 14px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align: justify; align-items: center; "><div id="conteudo" ><p><iframe id="video" height="315" width="500" src="' +
        url +
        '"  frameborder="0" allowfullscreen></iframe></p></div></div>'
    },

    //Escreva
    {
      title: "Escreva",
      image: "template2.gif",
      description: "Template de escreva.",
      html:
        '<style>#conteudo{-moz-column-count: 2;-webkit-column-count: 2; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; font-size 14px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align: justify;"><form action="javascript:func()" id="formulario_escreva" method="post"><div id="conteudo"><p style="text-align: left;">Conteudo aqui</p></div><input id="verificar" onclick="form_escreva()" style="background-color:#006d9a; border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF;" type="submit" value="Verificar" />&nbsp;</form></div>'
    },

    //fale
    {
      title: "Fale",
      image: "template2.gif",
      description: "Template de fale.",
      html:
        '<style>#conteudo{-moz-column-count: 2;-webkit-column-count: 2; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; font-size: 14px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align: justify;"><p><div id="conteudo"><p>conteudo aqui</p></div><p><button id="btnFalar" onclick="falar()" style="background-color:#006d9a; border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF;" type="button">Falar</button></p><div color="red" id="gravando">&nbsp;</div></p></div>'
    },

    //ouça
    {
      title: "Ouça",
      image: "template2.gif",
      description: "Template de fale.",
      html:
        '<style>#conteudo{-moz-column-count: 2;-webkit-column-count: 2; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; text-align: justify;"<div style="border: 0px red; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial;"><p style="text-align: left;"><font color="#000000" face="Verdana, serif" size="4"><span style="line-height: 28.8px;"><div id="conteudo" style=" border: 0px;" ><p>Conte&uacute;do Aqui</div></p><p><button onclick="ler()">Ou&ccedil;a</button><button onclick="stop_leitura()">Stop</button></p></div>'
    },

    // //3 colunas

    // //texto
    // {
    //   title: "Texto 3 Colunas",
    //   image: "template2.gif",
    //   description: "Template de texto.",
    //   html:
    //     '<style>#conteudo{-moz-column-count: 3;-webkit-column-count: 3; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; font-size: 14px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align: justify;"><div id="conteudo" style=" border: 0px;" ><p>Conte&uacute;do Aqui</div></p>'
    // },

    // //Video
    // {
    //   title: "Videos 3 Colunas",
    //   image: "template2.gif",
    //   description: "Template de video.",
    //   html:
    //     '<style>#conteudo{-moz-column-count: 3;-webkit-column-count: 3; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; font-size: 14px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align: justify; align-items: center; "><div id="conteudo" ><p><iframe id="video" height="315" width="500" src="' +
    //     url +
    //     '"  frameborder="0" allowfullscreen></iframe></p></div></div>'
    // },

    // //Escreva
    // {
    //   title: "Escreva 3 Colunas",
    //   image: "template2.gif",
    //   description: "Template de escreva.",
    //   html:
    //     '<style>#conteudo{-moz-column-count: 3;-webkit-column-count: 3; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; font-size 14px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align: justify;"><form action="javascript:func()" id="formulario_escreva" method="post"><div id="conteudo"><p style="text-align: left;">Conteudo aqui</p></div><input id="verificar" onclick="form_escreva()" style="background-color:#006d9a; border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF;" type="submit" value="Verificar" />&nbsp;&nbsp;</form></div>'
    // },

    // //fale
    // {
    //   title: "Fale 3 Colunas",
    //   image: "template2.gif",
    //   description: "Template de fale.",
    //   html:
    //     '<style>#conteudo{-moz-column-count: 3;-webkit-column-count: 3; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; font-size: 14px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align: justify;"><p><div id="conteudo"><p>conteudo aqui</p></div><p><img onclick="falar()" src="http://i.imgur.com/cHidSVu.gif" /></p><div color="red" id="gravando">&nbsp;</div></p></div>'
    // },

    // //ouça
    // {
    //   title: "Ouça 3 Colunas",
    //   image: "template2.gif",
    //   description: "Template de fale.",
    //   html:
    //     '<style>#conteudo{-moz-column-count: 3;-webkit-column-count: 3; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}</style><div style="overflow: auto; width: 100%; height: 100%; text-align: justify;"<div style="border: 0px red; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial;"><p style="text-align: left;"><font color="#000000" face="Verdana, serif" size="4"><span style="line-height: 28.8px;"><div id="conteudo" style=" border: 0px;" ><p>Conte&uacute;do Aqui</div></p><p><button onclick="ler()">Ou&ccedil;a</button><button onclick="stop_leitura()">Stop</button></p></div>'
    // }
  ]
});
