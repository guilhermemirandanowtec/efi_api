$(document).ready(function () {
    $('#celular').mask('(00) 00000-0000');
    $('#data_nascimento').mask('00/00/0000');
    $('#cpf').mask('000.000.000-00', { reverse: true });
    $('#cep').mask('00000-000');
});

