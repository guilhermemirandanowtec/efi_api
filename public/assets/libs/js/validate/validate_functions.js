// ***********************************************Validação Usuários***********************************************
function criarUsuario(page) {
    
    $("#formCriarUsuario").validate({
        // Regras de validação
        rules: {
            nome: {
                required: true,
                maxlength: 255,
            },
            email: {
                required: true,
                email: true,
            },
            celular: {
                required: true,
            },
            data_nascimento: {
                required: true,
            },
            senha: {
                required: function(){
                    if(page != "editar"){
                        return true;
                    }else{
                        return false;
                    }
                },
                maxlength: 255,
                minlength: 8,
                equalTo: "#reSenha"
            },
            reSenha: {
                required: function () {
                    if (page != "editar") {
                        return true;
                    } else {
                        return false;
                    }
                },
                minlength: 8,
                equalTo: "#senha"

            },
            cep: {
                required: true,
                maxlength: 255
            },
            cpf: {
                required: true,
                maxlength: 255
            },
            rua: {
                maxlength: 255
            },
            numero: {
                required: true,
                maxlength: 255
            },
            cidade: {
                maxlength: 255
            },
            estado: {
                maxlength: 255
            },
            bairro: {
                maxlength: 255
            },
            complemento: {
                maxlength: 255
            }
            
        },
        // Mensagens de erro
        messages: {
            nome: {
                required: "Campo nome não pode ser vazio!",
                maxlength: "Campo nome deve conter no máximo 10 caracteres!"
            },
            email: {
                required: "Campo e-mail não pode ser vazio!",
                email: "digite um e-mail válido!"
            },
            celular: {
                required: "Campo celular não pode ser vazio!"
            },
            data_nascimento: {
                required: "Campo data de nascimento não pode ser vazio!"
            },
            senha: {
                required: "Campo senha não pode ser vazio!",
                maxlength: "Campo senha deve conter no máximo 255 caracteres!",
                minlength: "Campo senha deve conter no minimo 8 caracteres!",
                equalTo: "Os campos de senhas devem ser iguais!"
            },
            reSenha: {
                required: "Campo Repetir Senha não pode ser vazio!",
                equalTo: "Os campos de senhas devem ser iguais!",
                minlength: "Campo Repetir Senha deve conter no minimo 8 caracteres!",

            },
            cep: {
                required: "Campo cep não pode ser vazio!",
                maxlength: "Campo cep deve conter no máximo 255 caracteres"
            },
            cpf: {
                required: "Campo cpf não pode ser vazio!",
                maxlength: "Campo cpf deve conter no máximo 255 caracteres"
            },
            rua: {
                maxlength: "Campo rua deve conter no máximo 255 caracteres"
            },
            numero: {
                required: "Campo numero não pode ser vazio!",
                maxlength: "Campo número deve conter no máximo 255 caracteres"
            },
            cidade: {
                maxlength: "Campo cidade deve conter no máximo 255 caracteres"
            },
            estado: {
                maxlength: "Campo estado deve conter no máximo 255 caracteres"
            },
            bairro: {
                maxlength: "Campo bairro deve conter no máximo 255 caracteres"
            },
            complemento: {
                maxlength: "Campo complemento deve conter no máximo 255 caracteres"
            }

        },
        // Se passar pelas validações faça o submit
        submitHandler: function (form) {
            $("#btn_submit").attr("disabled", true);
            $("#btn_submit").css("color", '#01638c');
            form.submit();
        }
    });
}

// ***********************************************Validação Cursos***********************************************

function criarCursoValidate(id_curso) {
    if ($('#img_change').attr('src') == ""){
        $("#FormCriarCurso").submit(function (event) {
            event.preventDefault();
        });
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Selecione uma imagem de capa!',
        });
    }else{
        $("#FormCriarCurso").validate({
            // Regras de validação
            rules: {
                idioma: {
                    required: true,
                },
                tituloModulo: {
                    required: true,
                    maxlength: 255,
                },
            
            },
            // Mensagens de erro
            messages: {
                idioma: {
                    required: "Campo idioma não pode ser vazio!",
                },
                tituloModulo: {
                    required: "Campo titulo não pode ser vazio!",
                    maxlength: "campo titulo deve conter no máximo 255 caracteres!"
                },
              
            },
            // Se passar pelas validações faça o submit
            submitHandler: function () {
                
                $("#btn_submit").attr("disabled", true);
                $("#btn_submit").css("color", '#01638c');
                
                if(id_curso == null){
                    salvarModulo();
                }else{
                    editarCurso(id_curso)
                }
    
            }
        });
    }
}

// ********************************************contrato validação*******************************************

$('#uploadContrato').on('change', function () {
    pdfValidate(this);
});

function pdfValidate(input) {
    let nome_pdf = input.files[0].name;
    if (nome_pdf.split('.')[1] != 'pdf'){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Este arquivo não é um PDF!',
        });
        // input.stop();
        $("#sendPDFButton").attr('disabled', true);
    }else{
        $("#sendPDFButton").attr("disabled", false);
    }
}


// ********************************************Login validação*******************************************
function validaCampoAdmin() {
    $("#form-login").validate({
        // Regras de validação
        rules: {
            senha: {
                required: true,
            },
            email: {
                required: true,
            },

        },
        // Mensagens de erro
        messages: {
            senha: {
                required: "Campo senha não pode ser vazio!",
            },
            email: {
                required: "Campo e-mail não pode ser vazio!",
            },

        },
        // Se passar pelas validações faça o submit
        submitHandler: function (form) {
            form.submit();
        }
    });
}

// ********************************************Feedback validação e Salvar*******************************************
function feedbackValidate(){
    let tipo_feedback = $('#type_feedback');
    let descricao_feedback = $('#about');

    
    if (!tipo_feedback.val()){

        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Selecione um tipo de feedback!',
        });

        tipo_feedback.focus();

    } else if (!descricao_feedback.val()){

        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Campo sobre feedback não pode ser vazio!',
        });

        sobre_feedback.focus();

    } else if (descricao_feedback.val().length > 255){

        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Campo sobre feedback não pode conter mais de 255 caracteres!',
        });

        sobre_feedback.focus();

    }else{
        $.ajax({
            url : BASE_URL + '/aluno/salvarFeedback',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                'tipo': tipo_feedback.val(),
                'descricao': descricao_feedback.val()
            }),
            processData: false,
            beforeSend: function(){
                $('#btn_send_feedback').attr('disabled', true);
            },
            success: function (data, textStatus, jQxhr) {
            
                $('#modal_feedback').modal('hide');
                Swal.fire({
                    type: 'success',
                    title: 'Bom trabalho...',
                    text: 'Feedback enviado com sucesso!',
                });
                
            },
            error: function (jqXhr, textStatus, errorThrown) {

                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Tivemos um problema para enviar o seu feedback!',
                });

            },
            complete: function(){
                $('#btn_send_feedback').attr('disabled', false);
                descricao_feedback.val('');
                tipo_feedback.val('');
            }

        });
    }
}

