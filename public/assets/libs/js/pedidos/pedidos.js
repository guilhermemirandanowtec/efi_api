$(function () {
  $(".input-search").keyup(function () {
    //pega o css da tabela
    var tabela = $(this).attr('alt');
    if ($(this).val() != "") {
      $("." + tabela + " tbody>tr").hide();
      $("." + tabela + " td:contains-ci('" + $(this).val() + "')").parent("tr").show();
    } else {
      $("." + tabela + " tbody>tr").show();
    }
  });
});
$.extend($.expr[":"], {
  "contains-ci": function (elem, i, match, array) {
    return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
  }
});

function formatDate(data) {

  var newData = new Date(data);
  var day = newData.getDate();
  var month = newData.getMonth() + 1;
  var year = newData.getFullYear();
  var hour = newData.getHours();
  var min = newData.getMinutes();
  var sec = newData.getSeconds();

  var array = [day, month, year, hour, min, sec];

  $.each(array, function (key, value) {
    if (value < '10') {
      array[key] = "0" + array[key]
    }
  });
  var date = array[0] + "/"
    + array[1] + "/"
    + array[2] + " "
    + array[3] + ":"
    + array[4] + ":"
    + array[5];

  return date;
}
function verMais(BASE_URL, id_pedido) {

  let status = {
    'approved': "Aprovado",
    'canceled': "Cancelado",
    'billet_printed': "Boleto Impresso",
    'refunded': "Devolvido",
    'dispute': "Em Disputa",
    'completed': "Completo",
    'blocked': "Bloqueado",
    'chargeback': "Estornado",
    'delayed': "Atrasado",
    'expired': "Expirou"
  };
  
  $('#app').html('');
  axios({
    method: 'get',
    url: BASE_URL + '/compra/getInfo/' + id_pedido,
  })
    .then(function (response) {
      if (response.data == '') {
        Swal.fire({
          title: 'Opa!',
          text: "Não recebemos informações sobre seu pedido. Por favor, volte mais tarde e tente novamente!",
          type: 'warning',
          confirmButtonColor: '#01638c',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if (result.value) {
            location.reload();
          }
        })
      } else {
        $('#app').append(
          '<div id="bino" class="tituloPedido">'
          + '<div class="row">'
          + '<div class="col col-md-6" >'
          + '<label for="" style="color: white;">Nome do Pacote: </label>'
          + '<span style="color: white; font-size: 15px;">  ' + response.data[0].descricao + '</span>'
          + '</div >'
          + '<div class="col col-md-6">'
          + '<label for="" style="color: white;">Preço Pago: </label>'
          + '<span style="color: white; font-size: 15px;"> R$' + response.data[0].valor + '</span>'
          + '</div>'
          + '</div >'
          + '</div>'
        );
        $.each(response.data, function (index, el) {
          if (index % 2 == 0) {
            var cor = "cor-sim";
          } else {
            var cor = "cor-nao";
          }
          var data = formatDate(el.purchase_date);
          $('#app').append(
            '<div id="bino" class=' + cor + '>'
            + '<div class="row">'
            + '<div class="col col-md-6">'
            + '<label for="">Código Compra: </label>'
            + '<span>  ' + el.transaction + '</span>'
            + '</div>'
            + '<div class="col col-md-6" >'
            + '<label for="">Data Compra: </label>'
            + '<span>  ' + data + '</span>'
            + '</div >'
            + '</div >'
            + '<div class="row">'
            + '<div class="col col-md-6">'
            + '<label for="">Status do Pedido: </label>'
            + '<span> ' + status[el.status] + '</span>'
            + '</div>'
            + '<div class="col col-md-6">'
            + '<label for="">Data de Confirmação: </label>'
            + '<span>' + (el.confirmation_purchase_date == null ? ' Sem Confirmação' : el.confirmation_purchase_date) + '</span>'
            + '</div>'
            + '</div>'
            + '</div>'
          );
          $('#modalSaibaMais').modal('show');
        });
      }

    })
    .catch(function (error) {
      console.warn(error);
    })
    .finally(function () {
    });


}

function continuarCompra(BASE_URL, id_pedido) {
  $('#app').html('');
  axios({
    method: 'get',
    url: BASE_URL + '/compra/continuarCompra/' + id_pedido,
  })
    .then(function (response) {
      // console.log(response.data);
      // location.href = response.data;
      if (response.data == '') {
        Swal.fire({
          title: 'Opa!',
          text: "Não recebemos informações sobre seu pedido. Por favor, volte mais tarde e tente novamente!",
          type: 'warning',
          confirmButtonColor: '#01638c',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if (result.value) {
          }
        })
      } else {
        location.href = response.data;
      }

    })
    .catch(function (error) {
      console.warn(error);
    })
    .finally(function () {
    });


}
