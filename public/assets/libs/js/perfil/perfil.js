$(document).ready(function () {
  $('.dateMask').mask('99/99/9999');
  $('.cepMask').mask('99999-999');
  $('.celularMask').mask('(99) 99999-9999');
  $('.CPFMask').mask('999.999.999-99');
});

// Function do modal
$('#modalTrocarSenha').on('hidden.bs.modal', function (e) {
  location.reload();
})
$('#modalTrocarFoto').on('hidden.bs.modal', function (e) {
  location.reload();
})
// Function do modal

function trocarSenha(BASE_URL) {
  $("#loadingsaveSenha").css('display', 'block');
  $(".campoSenhaDiff").css("display", "none");
  var senha = $("#nova-senha").val();
  var senhaC = $("#repetir-senha").val();
  if (senha != senhaC) {
    $(".campoSenhaDiff").css("display", "inline-table");
  } else {
    $("#result-senha").css("display", "none");
    $.ajax({
      url: BASE_URL + '/aluno/alterarSenha',
      dataType: 'html',
      type: 'post',
      contentType: 'application/json',
      data: JSON.stringify({
        "senha-atual": $("#senha-atual").val(),
        "senha-nova": $("#nova-senha").val(),
        "repetir-senha": $("#repetir-senha").val()
      }),

    }).done(function (resposta) {
      $("#loadingsaveSenha").css('display', 'none');
      if (resposta == 'atualizado') {
        Swal.fire({
          title: 'Sucesso!',
          text: "Senha atualizada!",
          type: 'success',
          confirmButtonColor: '#01638c',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if (result.value) {
            $('#modalTrocarSenha').modal('hide')
            location.reload();
          }
        })
      }
      if (resposta == 'incorreta') {
        $(".resultErrorModal").css("display", "none");
        $("#result-senha").html('Senha atual está incorreta. tente novamente!');
        $("#result-senha").css("display", "inline-table");
      }

      if (resposta != 'atualizado' && resposta != 'incorreta') {
        $(".resultErrorModal").html(resposta);
        $("span.gump-error-message").each(function () {
          $(this).append("<br>");
        });
        $(".resultErrorModal").css('display', 'block');
      }

    }).fail(function (jqXHR, textStatus) {
      console.log("Request failed: " + textStatus);

    });
  }
}


function validaEmail(BASE_URL) {
  $("#loading").css("display", "inline-table");
  $("#emailIndisponivel").css("display", "none");
  $("#emailDisponivel").css("display", "none");
  $.ajax({
    url: BASE_URL + '/aluno/validaEmail',
    dataType: 'html',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({
      "email": $("#email").val()
    }),

  }).done(function (resposta) {
    $("#loading").css("display", "none");
    if (resposta == 'emailIndisponivel') {
      $("#emailIndisponivel").css("display", "inline-table");
      $("#email").focus();
    } else {
      $("#emailDisponivel").css("display", "inline-table");
    }
  }).fail(function (jqXHR, textStatus) {
    console.log("Request failed: " + textStatus);

  }).always(function () {
    console.log("completou");
  });
}

function salvar(BASE_URL) {
  $("#loadingsave").css('display', 'block');
  $.ajax({
    url: BASE_URL + '/aluno/atualizaPerfil',
    dataType: 'html',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({
      "nome": $("#nome").val(),
      "email": $("#email").val(),
      "celular": $("#celular").val(),
      "estado-civil": $("#estado-civil option:selected").val(),
      "data-nascimento": $("#data-nascimento").val(),
      "cpf": $("#cpf").val(),
      "sexo": $("#sexo option:selected").val(),
      "cep": $("#cep").val(),
      "rua": $("#rua").val(),
      "numero": $("#numero").val(),
      "cidade": $("#cidade").val(),
      "estado": $("#estado").val(),
      "bairro": $("#bairro").val(),
      "complemento": $("#complemento").val()
    }),

  }).done(function (resposta) {
    $("#loadingsave").css('display', 'none');
    if (resposta == 'atualizado') {
      Swal.fire({
        title: 'Sucesso!',
        text: "Perfil atualizado!",
        type: 'success',
        confirmButtonColor: '#01638c',
        confirmButtonText: 'Ok',
      }).then((result) => {
        if (result.value) {
          location.reload();
        }
      })
    } else {
      $(".resultError").html(resposta);
      $("span.gump-error-message").each(function () {
        $(this).append("<br>");
      });
      $(".resultError").css('display', 'block');
      $('html, body').animate({ scrollTop: $('.container-fluid').offset().top }, 500);
    }

  }).fail(function (jqXHR, textStatus) {
    console.log("Request failed: " + textStatus);

  }).always(function () {
    console.log("completou");
  });

}

function trocaFoto(BASE_URL) {
  $("#loadingsaveFoto").css('display', 'block');
  $(".resultErrorModalFoto").css('display', 'none');
  $("#resultErrorFoto").css('display', 'none');
  var formData = new FormData();

  var fileSelect = document.getElementById("arquivo");
  if (fileSelect.files && fileSelect.files.length == 1) {
    var file = fileSelect.files[0]
    formData.set("file", file, file.name);
  }

  $.ajax({
    url: BASE_URL + '/aluno/trocaFoto',
    data: formData,
    type: 'POST',
    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
    processData: false, // NEEDED, DON'T OMIT THIS

  }).done(function (resposta) {
    $("#loadingsaveFoto").css('display', 'none');
    if (resposta == 'ok') {
      $('#modalTrocarFoto').modal('hide')
      Swal.fire({
        title: 'Sucesso!',
        text: "Perfil atualizado!",
        type: 'success',
        confirmButtonColor: '#01638c',
        confirmButtonText: 'Ok',
      }).then((result) => {
        if (result.value) {
          location.reload();
        }
      })
    } else if (resposta == 'imgGrande') {
      $(".resultErrorModalFoto").html('O tamanho da foto é muito grande.');
      $(".resultErrorModalFoto").css('display', 'block');
    } else {
      $("#resultErrorFoto").html(resposta);
      $("span.gump-error-message").each(function () {
        $(this).append("<br>");
      });
      $("#resultErrorFoto").css('display', 'block');
    }

  }).fail(function (jqXHR, textStatus) {
    console.log("Request failed: " + textStatus);

  });


}

function validaCPF(cpf, BASE_URL) {

  $("#cpfValido").css("display", "none");
  $("#cpfInvalido").css("display", "none");
  let result = 0;
  result = funcValida(cpf);
  if (result === true) {
    $("#cpfValido").css("display", "inline-table");
    validaCpfUso(BASE_URL);
  } else {
    $("#cpfInvalido").css("display", "inline-table");
    $("#cpfEmUso").css("display", "none");
    $("#cpf").val("");
  }
}

function funcValida(cpf) {
  cpf = cpf.replace(/[^\d]+/g, '');
  if (cpf == '') return false;
  // Elimina CPFs invalidos conhecidos
  if (cpf.length != 11 ||
    cpf == "00000000000" ||
    cpf == "11111111111" ||
    cpf == "22222222222" ||
    cpf == "33333333333" ||
    cpf == "44444444444" ||
    cpf == "55555555555" ||
    cpf == "66666666666" ||
    cpf == "77777777777" ||
    cpf == "88888888888" ||
    cpf == "99999999999")
    return false;
  // Valida 1o digito
  add = 0;
  for (i = 0; i < 9; i++)
    add += parseInt(cpf.charAt(i)) * (10 - i);
  rev = 11 - (add % 11);
  if (rev == 10 || rev == 11)
    rev = 0;
  if (rev != parseInt(cpf.charAt(9)))
    return false;
  // Valida 2o digito
  add = 0;
  for (i = 0; i < 10; i++)
    add += parseInt(cpf.charAt(i)) * (11 - i);
  rev = 11 - (add % 11);
  if (rev == 10 || rev == 11)
    rev = 0;
  if (rev != parseInt(cpf.charAt(10)))
    return false;
  return true;
}

function validaCpfUso(BASE_URL) {
  $("#loadingCPF").css("display", "inline-table");
  $("#cpfValido").css("display", "none");
  $("#cpfInvalido").css("display", "none");
  $.ajax({
    url: BASE_URL + '/aluno/validaCPF',
    dataType: 'html',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({
      "cpf": $("#cpf").val()
    }),

  }).done(function (resposta) {
    $("#loadingCPF").css("display", "none");
    if (resposta == 'cpfEmUso') {
      $("#cpfEmUso").css("display", "inline-table");
      $("#cpf").val("");
    }
    else if (resposta == 'cpfOK') {
      $("#cpfEmUso").css("display", "none");
      $("#cpfValido").css("display", "inline-table");
    } else {
      $("#cpfEmUso").css("display", "none");
      $("#cpfValido").css("display", "inline-table");
    }
  }).fail(function (jqXHR, textStatus) {
    console.log("Request failed: " + textStatus);
  });
}
