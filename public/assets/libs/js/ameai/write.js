
function getWriteAnswear(id_texto) {

    localStorage.setItem('answears', []);

    $.ajax({

        url: '/api/getWriteAnswear/',
        dataType: 'json',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({
            "id_texto": id_texto
        }),
        processData: false,
        success: function (data, textStatus, jQxhr) {
          console.log(data['Resposta']);

            localStorage.setItem('answears', data['Resposta']);
            $('.loading-ameai').hide();

        },
        error: function (jqXhr, textStatus, errorThrown) {

            console.log(errorThrown);
            $('.loading-ameai').hide();

        }

    });

}

function form_escreva(refazerExercicio = false) {
  console.log('entrou');

    let countInputs = $("#formulario_escreva").find("input").length;
    let answears = localStorage.getItem('answears').split('*');
    let quan_frases = countInputs - 1;
    let countCorrectAnswear = 0;

    for (let index = 0; index < quan_frases; index++) {

        if(refazerExercicio) {

            objetoCorrecao = ".swal2-popup input[name=" + (index + 1) + "]";

        } else {

            objetoCorrecao = "input[name=" + (index + 1) + "]";

        }

        if ($(objetoCorrecao).val() != undefined && $(objetoCorrecao).val() != "" && answears[index].toLowerCase() == $(objetoCorrecao).val().toLowerCase().trim()) {

            $(objetoCorrecao).addClass('correct-answear');
            $(objetoCorrecao).removeClass('wrong-answear');
            $(objetoCorrecao).prop("disabled", true);
            countCorrectAnswear++;

        } else if ($(objetoCorrecao).val().trim() != "") {

            $(objetoCorrecao).addClass('wrong-answear');

        }

    }

    countCorrectAnswear = (100 * countCorrectAnswear) / quan_frases;
    localStorage.setItem('nota', countCorrectAnswear);

    if (countCorrectAnswear >= 70) {

        if (refazerExercicio) {

            $('#verificar').replaceWith('<input id="verificar" onclick="form_escreva()" style="background-color:#006d9a;border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF;" type="submit" value="Verificar">');

        } else {

            $('#verificar').replaceWith('<input id="verificar" onclick="form_escreva()" style="background-color:#006d9a;border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF;" type="submit" value="Verificar"> <input id="finalizar_exercicio" onclick="finalizar_escreva()" style="background-color:#006d9a; border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF;" type="button" value="Finalizar exercício">');

        }

    }

}

function finalizar_escreva() {

    let nota = localStorage.getItem('nota');
    let inputName = 0;
    let valueAttr = '';

    $("#formulario_escreva [type=text]").each(function (i) {

        inputName = i + 1;
        valueAttr = $('input[name=' + inputName + ']').val().trim();

        $(this).attr('value', valueAttr);
        $(this).attr("disabled", true);

    });

    $("#formulario_escreva [type=submit]").replaceWith("<span class='writeGrade'> Exercício concluído com " + nota + "% de acertos</span>");
    $("#formulario_escreva [type=button]").remove();
    $("#formulario_escreva button").remove();

    let texto = $('.content-text').prop('outerHTML');

    $('#finalizar_exercicio').attr("disabled", true);

    window.scrollTo(0, 0);

    $('.loading-ameai').show();

    $.ajax({

        url: '/api/finalizar_escreva/',
        dataType: 'JSON',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({

            id_usuario: id_usuario,
            id_texto: localStorage.getItem('id_texto'),
            nota: nota,
            texto: texto

        }),
        processData: false,
        success: function (data, textStatus, jQxhr) {

            if(data % 2 == 0) {

                sumario = (data / 2);

                if ($(".check_undone[name='" + sumario  + "']").hasClass('fa-circle')) {

                    $(".check_undone[name='" + sumario  + "']").addClass('check_done');
                    $(".check_undone[name='" + sumario  + "']").addClass('fa-check')
                    $(".check_undone[name='" + sumario  + "']").removeClass('fa-circle');
                    $(".check_undone[name='" + sumario  + "']").removeClass('check_undone');

                }

            }

            localStorage.setItem('answears', data['Resposta']);

            // changeTab(5);

            $('.loading-ameai').hide();

        },
        error: function (jqXhr, textStatus, errorThrown) {

            console.log(errorThrown);
            $('html, body').css({
                'overflow': 'initial'
            });
            $('.loading-ameai').hide();

        }

    });

}
