const audio = document.createElement('audio');
let mediaRecorder;
let localStream;
let nota;
let recognizer;
localStorage.setItem('nota', '0');


function gravarAudio(callback) {

    navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {

        localStream = stream;
        window.streamReference = stream;

        let chunks = [];

        mediaRecorder = new MediaRecorder(stream)

        mediaRecorder.ondataavailable = data => {

            chunks.push(data.data);

        }

        callback();

        mediaRecorder.onstop = () => {

            const blob = new Blob(chunks, { type: 'audio/ogg; code=opus' });
            const reader = new window.FileReader();

            reader.readAsDataURL(blob);

            reader.onloadend = () => {

                audio.src = reader.result;
                audio.controls = true;

            }

        }

    }, err => {

        alert('Impossível acessar o microfone');

    })

};

function falar(refazerExercicio = false) {
  let input = $('input[type=text]');

  var arr = [];

  input.map(function(index, item){
    arr.push({item:$(item).val()});
  })

  var button = document.getElementById('btnFalar');

  var html = $('.content-text').prop('outerHTML');

  var id_texto = $('#id_texto').val();

  window.ReactNativeWebView.postMessage(JSON.stringify({words:arr, html:html, button:button.outerHTML, id_texto:id_texto}))
    // exercicio(1, true, refazerExercicio);

}

function exercicio(phraseIndex = 1, animation = true, refazerExercicio) {

    localStorage.setItem('phraseIndex', phraseIndex)

    let input = $('input[type=text][name=' + phraseIndex + ']');
    let questionsLength = $('.content-text input[type=text]').length;
    let prevEnable = phraseIndex <= 1 ? 'disabled' : 'enabled';
    let nextEnable = phraseIndex >= questionsLength ? 'disabled' : 'enabled';
    let prevIndex = parseInt(phraseIndex) - 1;
    let nextIndex = parseInt(phraseIndex) + 1;
    let displayMic = input.hasClass('correct-answear') && !refazerExercicio ? 'none' : 'initial';
    let type = input.hasClass('correct-answear') ? 'success' : 'question';
    let title = input.hasClass('correct-answear') ? 'Pronúncia correta' : 'Exercício Fale';
    let readPhrase = input.val().replace("'", "\\\'");

    html = "<h4>Frase do Exercício:</h4>" +
        "<p>" +
        input.val() +
        "<span style='margin-left:5px;display:" + displayMic + "' onclick='falarFrase(" + refazerExercicio + ")'><i class='fas fa-microphone recording-mic'></i></span>" +
        "</p>" +
        "<button onclick='exercicio(" + prevIndex + ", false, " + refazerExercicio + ")' data-toggle='tooltip' data-placement='top' title='Frase anterior' class='btn btn-listening-control btn-circle' " + prevEnable + "><i class='fas fa-arrow-left'></i></button>" +
        "<button onclick='audio.play()' data-toggle='tooltip' data-placement='top' title='Ouvir tentativa' class='btn btn-listening-control btn-circle' disabled><i class='fas fa-volume-up'></i></button>" +
        "<button onclick=\"listeningTalkQuestion("
            .concat("'", readPhrase, "'");

    html = html + ")\" data-toggle='tooltip' data-placement='top' title='Ouvir pronúncia' class='btn btn-listening-control btn-circle' ><i class='fas fa-play'></i></i></button>" +
        "<button onclick='exercicio(" + nextIndex + ", false, " + refazerExercicio + ")' data-toggle='tooltip' data-placement='top' title='Próxima frase' class='btn btn-listening-control btn-circle' " + nextEnable + "><i class='fas fa-arrow-right'></i></button>";

    Swal.fire({
        type: type,
        title: title,
        html: html,
        animation: animation,
        confirmButtonText: 'OK',
        confirmButtonColor: '#01638c',
        showCloseButton: true,
        showConfirmButton: false,
        onClose: () => {

            if (recognizer != undefined) {

                recognizer.stop();

            }

            stopRecordVoice();

        }

    });

}

function falarFrase(refazerExercicio) {

    let phraseIndex = localStorage.getItem('phraseIndex');

    window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || null;

    if (window.SpeechRecognition === null) {

        Swal.fire(
            'Recurso não suportado',
            'O navegador que você está utilizando não suporta esse recurso. <br> Tente novamente em outro navegador (Recomendado Google Chrome).',
            'error'
        )

        return FALSE;

    } else {

        if ($('.recording-mic').hasClass('rec')) {

            $('.recording-mic').removeClass('rec');

            if (recognizer != undefined) {

                recognizer.stop();

            }

            stopRecordVoice();

            return false;

        } else {

            $('.recording-mic').addClass('rec');

            recognizer = new window.SpeechRecognition();

            recognizer.continuous = true;
            recognizer.lang = 'en-US';

            try {

                this.gravarAudio(function () { mediaRecorder.start() });

                recognizer.start();

            } catch (ex) {

                alert("error: " + ex.message);

            }

            recognizer.onresult = function (event) {

                $('.recording-mic').removeClass('rec');
                mediaRecorder.stop()

                for (var i = event.resultIndex; i < event.results.length; i++) {

                    if (event.results[i].isFinal) {

                        if (recognizer != undefined) {

                            recognizer.stop();

                        }

                        modalTalk(event.results[i][0].transcript, phraseIndex, false, refazerExercicio)

                    }

                }

            }

            recognizer.onaudioend = function () {

                $('.recording-mic').removeClass('rec');

            }

        }

    }

}



function corrigeFala(phraseIndex, transcript, refazerExercicio) {

    let input = $('.content-text input[type=text][name=' + phraseIndex + ']');

    transcript = phraseSimplify(transcript);

    if (phraseSimplify(input.val()) == transcript) {

        if (input.hasClass('wrong-answear')) {

            input.removeClass('wrong-answear');

        }

        input.addClass('correct-answear');

        if (!refazerExercicio) {

            addFinishButton();

        }

        return true;

    } else {

        if (input.hasClass('correct-answear')) {

            input.removeClass('correct-answear');

        }

        input.addClass('wrong-answear');

        return false;

    }

}

function modalTalk(transcript, phraseIndex = 1, animation = false, refazerExercicio) {

    let correcao = corrigeFala(phraseIndex, transcript, refazerExercicio);
    let questionsLength = $('.content-text input[type=text]').length;
    let inputVal = $('.content-text input[type=text][name=' + phraseIndex + ']').val();
    let prevEnable = phraseIndex <= 1 ? 'disabled' : 'enabled';
    let nextEnable = phraseIndex >= questionsLength ? 'disabled' : 'enabled';
    let prevIndex = parseInt(phraseIndex) - 1;
    let nextIndex = parseInt(phraseIndex) + 1;
    let title = correcao ? 'Pronúncia correta' : 'Pronúncia incorreta';
    let type = correcao ? 'success' : 'error';
    let readPhrase = inputVal.replace("'", "\\\'");
    let html = '';
    let displayMic = correcao ? 'none' : 'initial';

    html = html +
        "<h3>Você falou:</h3><p>" + transcript +
        "<span style='margin-left:5px;display:" + displayMic + "' onclick='falarFrase(" + refazerExercicio  + ")'><i class='fas fa-microphone recording-mic'></i></span></p>" +
        "<h3>Frase correta: </h3>" +
        "<p>" + inputVal + "</p>" +
        "<button onclick='exercicio(" + prevIndex + ", false, " + refazerExercicio + ")' data-toggle='tooltip' data-placement='top' title='Frase anterior' class='btn btn-listening-control btn-circle' " + prevEnable + "><i class='fas fa-arrow-left'></i></button>" +
        "<button onclick='audio.play()' data-toggle='tooltip' data-placement='top' title='Ouvir tentativa' class='btn btn-listening-control btn-circle' ><i class='fas fa-volume-up'></i></button>" +
        "<button onclick=\"listeningTalkQuestion("
            .concat("'", readPhrase, "'");

    html = html + ")\" data-toggle='tooltip' data-placement='top' title='Ouvir pronúncia' class='btn btn-listening-control btn-circle'><i class='fas fa-play'></i></i></button>" +
        "<button onclick='exercicio(" + nextIndex + ", false, " + refazerExercicio + ")' data-toggle='tooltip' data-placement='top' title='Próxima frase' class='btn btn-listening-control btn-circle' " + nextEnable + "><i class='fas fa-arrow-right'></i></button>";

    stopRecordVoice();

    Swal.fire({
        type: type,
        title: title,
        html: html,
        animation: animation,
        confirmButtonText: 'OK',
        confirmButtonColor: '#01638c',
        showCloseButton: true,
        showConfirmButton: false,
        onClose: () => {

            if (recognizer != undefined) {

                recognizer.stop();

            }

            stopRecordVoice();

        }
    });

}

function listeningTalkQuestion(phrase) {

    msg = new SpeechSynthesisUtterance();

    msg.text = phrase;
    msg.voice = voices[1];
    msg.rate = 0.5;

    window.speechSynthesis.speak(msg);

}

function addFinishButton() {

    let correctAnswearLen = $('input[type=text][class=correct-answear]').length;
    let questionsLen = $('input[type=text]').length;

    let percent = ((100 * correctAnswearLen) / questionsLen).toFixed(2);

    localStorage.setItem('nota', percent);

    if ($('#btnFinalizarTalk').length <= 0) {

        if (percent >= 70) {

            let button = '<input id="btnFinalizarTalk" onclick="finalizar_fale()" style="background-color:#006d9a; border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF; margin-left:10px;" type="button" value="Finalizar Exercício"></input>'

            $(button).insertAfter('#btnFalar');

        }

    }

}

function finalizar_fale() {

    let nota = localStorage.getItem('nota');

    $('#btnFalar').replaceWith("<span class='writeGrade'> Exercício concluído com " + nota + "% de acertos</span>");
    $('#btnFinalizarTalk').remove();

    let texto = $('.content-text').prop('outerHTML');

    window.scrollTo(0, 0);

    $('.loading-ameai').show();

    $.ajax({

        url: '/api/finalizar_fale/',
        dataType: 'JSON',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({

            id_usuario: id_usuario,
            id_texto: localStorage.getItem('id_texto'),
            nota: nota,
            texto: texto

        }),
        processData: false,
        success: function (data, textStatus, jQxhr) {

            if(data % 2 == 0) {

                sumario = (data / 2);

                if ($(".check_undone[name='" + sumario  + "']").hasClass('fa-circle')){

                    $(".check_undone[name='" + sumario  + "']").addClass('check_done');
                    $(".check_undone[name='" + sumario  + "']").addClass('fa-check')
                    $(".check_undone[name='" + sumario  + "']").removeClass('fa-circle');
                    $(".check_undone[name='" + sumario  + "']").removeClass('check_undone');

                }

            }

            localStorage.setItem('answears', data['Resposta']);

            changeTab(2);

            $('.loading-ameai').hide();

        },
        error: function (jqXhr, textStatus, errorThrown) {

            console.log(errorThrown);
            $('html, body').css({
                'overflow': 'initial'
            });
            $('.loading-ameai').hide();

        }

    });

}

function stopRecordVoice() {

    if (window.streamReference != undefined) {

        window.streamReference.getAudioTracks().forEach(function (track) {

            track.stop();

        });

    }
}
