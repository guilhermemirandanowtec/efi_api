// VALIDAÇÃO DO FRONTEND
function validaCampo() {

  $(".compoEmail").css("display", "none");
  $(".compoSenha").css("display", "none");
  $(".inputEmail").removeClass("inputEmailErro");
  $(".inputSenha").removeClass("inputSenhaErro");

  var email = $('.inputEmail').val();
  var senha = $('.inputSenha').val();

  if (email == '' && senha == '') {
    $(".compoEmail").css("display", "block");
    $(".compoSenha").css("display", "block");
    $(".inputEmail").addClass("inputEmailErro");
    $(".inputSenha").addClass("inputSenhaErro");
  } else if (email == '' && !senha == '') {
    $(".compoEmail").css("display", "block");
    $(".inputEmail").addClass("inputEmailErro");
  } else if (!email == '' && senha == '') {
    $(".compoSenha").css("display", "block");
    $(".inputSenha").addClass("inputSenhaErro");
  } else {
    $("#form-login").submit();
  }
}

$(document).ready(function () {
  // VALIDAÇÃO DO BACKEND
  var url = window.location.href;
  var param1 = url.split("?")[1];
  var param2 = url.split("?")[2];

  if (typeof param2 === "undefined") {
  } else {
    var email = atob(param2);
    $(".inputEmail").val(email);
  }
  if (typeof param1 === "undefined") {
    // console.log('safe');
  } else {
    var decodeErro = atob(param1);
    switch (decodeErro) {
      case "campoEmailSenha":
        $(".compoEmail").css("display", "block");
        $(".compoSenha").css("display", "block");
        $(".inputEmail").addClass("inputEmailErro");
        $(".inputSenha").addClass("inputSenhaErro");
        break;
      case "campoEmail":
        $(".compoEmail").css("display", "block");
        $(".inputEmail").addClass("inputEmailErro");
        break;
      case "campoSenha":
        $(".compoSenha").css("display", "block");
        $(".inputSenha").addClass("inputSenhaErro");
        break;
      case "loginInvalido":
        swal.fire("Ops!", "Credenciais incorretas. Por favor, verifique-as e tente novamente!", "error");
        break;
      case "registroCriado":
        swal.fire("Good Jobs!", "Cadastro criado com sucesso. Por favor, realize seu login!", "success");
        break;
      default:
        swal.fire("Deu Ruim!", "", "error");
    }
  }
});
