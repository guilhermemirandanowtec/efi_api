$(document).ready(function () {

    mobileSingleColumn();

});

function disableInputText() {

    if ($('input[type=text]').length > 0) {

        $('input[type=text]').attr('disabled', true);

    }

}

function mobileSingleColumn() {

    if ($(window).width() <= 768) {

        texto = $('.content-text').html();

        $('.content-text').html(

            texto.replace(
                '#conteudo{-moz-column-count: 2;-webkit-column-count: 2; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}',
                '#conteudo{-moz-column-count: 1;-webkit-column-count: 1; -o-column-rule:1px dotted #006d9a; -webkit-column-rule:1px dotted #006d9a;}'
            )

        );

    }

}

function phraseSimplify(phrase) {

    let find = ["ã", "à", "á", "ä", "â", "è", "é", "ë", "ê", "ì", "í", "ï", "î", "ò", "ó", "ö", "ô", "ù", "ú", "ü", "û", "ñ", "ç"]; "à", "á", "ä", "â", "è", "é", "ë", "ê", "ì", "í", "ï", "î", "ò", "ó", "ö", "ô", "ù", "ú", "ü", "û", "ñ", "ç"
    let replace = ["a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "u", "u", "u", "u", "n", "c"];

    for (let i = 0; i < find.length; i++) {

        phrase = phrase.replace(new RegExp(find[i], 'gi'), replace[i]);

    }

    phrase = phrase.replace(/[^\w\s]/gi, '');
    phrase = phrase.replace('?', '');
    phrase = phrase.replace('!', '');
    phrase = phrase.replace('.', '');
    phrase = phrase.replace('/', '');

    numeroFrase = phrase.replace(/\D+/g, '|');
    numeroFrase = numeroFrase.split('|');

    $.each(numeroFrase, function (index, value) {

        phrase = phrase.replace(value, convertNumberToWords(value));

    });

    return phrase.trim().toLowerCase().replace(/\s{2,}/g, ' ');

}

function convertNumberToWords(amount) {

    let words = new Array();

    words[0] = '';
    words[1] = 'one';
    words[2] = 'two';
    words[3] = 'three';
    words[4] = 'four';
    words[5] = 'five';
    words[6] = 'six';
    words[7] = 'seven';
    words[8] = 'eight';
    words[9] = 'nine';
    words[10] = 'ten';
    words[11] = 'eleven';
    words[12] = 'twelve';
    words[13] = 'thirteen';
    words[14] = 'fourteen';
    words[15] = 'fifteen';
    words[16] = 'sixteen';
    words[17] = 'seventeen';
    words[18] = 'eighteen';
    words[19] = 'nineteen';
    words[20] = 'twenty';
    words[30] = 'thirty';
    words[40] = 'forty';
    words[50] = 'fifty';
    words[60] = 'sixty';
    words[70] = 'seventy';
    words[80] = 'eighty';
    words[90] = 'ninety';

    amount = amount.toString();

    let atemp = amount.split(".");
    let number = atemp[0].split(",").join("");
    let n_length = number.length;
    let words_string = "";

    if (n_length <= 9) {

        let n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        let received_n_array = new Array();

        for (let i = 0; i < n_length; i++) {

            received_n_array[i] = number.substr(i, 1);

        }

        for (let i = 9 - n_length, j = 0; i < 9; i++ , j++) {

            n_array[i] = received_n_array[j];

        }

        for (let i = 0, j = 1; i < 9; i++ , j++) {

            if (i == 0 || i == 2 || i == 4 || i == 7) {

                if (n_array[i] == 1) {

                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;

                }

            }

        }

        value = "";

        for (let i = 0; i < 9; i++) {

            if (i == 0 || i == 2 || i == 4 || i == 7) {

                value = n_array[i] * 10;

            } else {

                value = n_array[i];

            }

            if (value != 0) {

                words_string += words[value] + " ";

            }

            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {

                words_string += "Crores ";

            }

            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {

                words_string += "Lakhs ";

            }

            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {

                words_string += "Thousand ";

            }

            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {

                words_string += "Hundred and ";

            } else if (i == 6 && value != 0) {

                words_string += "Hundred ";

            }

        }

        words_string = words_string.split("  ").join(" ");

    }

    return words_string;

}