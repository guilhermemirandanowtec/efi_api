// VALIDAÇÃO DO FRONTEND
function validaCampo() {

  let Erro = 0;
  var nome = $('.inputNome').val();
  var email = $('.inputEmail').val()
  var senha = $('.inputSenha').val();
  var senhaC = $('.inputSenhaC').val();
  var termos = $('.termos').is(":checked");

  // console.log(termos);

  $(".campoNome").css("display", "none");
  $(".inputNome").removeClass("inputNomeErro");

  $(".campoEmail").css("display", "none");
  $(".inputEmail").removeClass("inputEmailErro");

  $(".campoSenha").css("display", "none");
  $(".inputSenha").removeClass("inputSenhaErro");

  $(".campoSenhaC").css("display", "none");
  $(".inputSenhaC").removeClass("inputSenhaCErro");

  $(".campoSenhaDiff").css("display", "none");
  $(".campoSenhaMin").css("display", "none");
  $(".campoTermos").css("display", "none");

  $(".campoEmailInvalido").css("display", "none");
  $(".inputEmail").removeClass("inputEmailErro");

  if (nome == '') {
    $(".campoNome").css("display", "block");
    $(".inputNome").addClass("inputNomeErro");
    Erro++;
  }
  if (email == '') {
    $(".campoEmail").css("display", "block");
    $(".inputEmail").addClass("inputEmailErro");
    // $(".campoEmailInvalido").css("display", "none");
    Erro++;
  }
  if (!email == '') {

    usuario = email.substring(0, email.indexOf("@"));
    dominio = email.substring(email.indexOf("@")+ 1, email.length);
    if ((usuario.length >=1) &&
    (dominio.length >=3) &&
    (usuario.search("@")==-1) &&
    (dominio.search("@")==-1) &&
    (usuario.search(" ")==-1) &&
    (dominio.search(" ")==-1) &&
    (dominio.search(".")!=-1) &&
    (dominio.indexOf(".") >=1)&&
    (dominio.lastIndexOf(".") < dominio.length - 1)) {
      $(".campoEmail").css("display", "none");
      $(".campoEmailInvalido").css("display", "none");
      $(".inputEmail").removeClass("inputEmailErro");
    }
    else{
      Erro++;
      $(".campoEmail").css("display", "none");
      $(".campoEmailInvalido").css("display", "block");
      $(".inputEmail").addClass("inputEmailErro");
    }
  }
  if (senha == '') {
    $(".campoSenha").css("display", "block");
    $(".inputSenha").addClass("inputSenhaErro");
    Erro++;
    console.log(Erro, "4");
  }
  if (senhaC == '') {
    $(".campoSenhaC").css("display", "block");
    $(".inputSenhaC").addClass("inputSenhaCErro");
    Erro++;
  }

  if (!senha == '' && !senhaC == '') {
    if (senha === senhaC) {
      if (senha.length < 6) {
        $(".campoSenhaMin").css("display", "block");
        Erro++;
      }
    } else {
      $(".campoSenhaDiff").css("display", "block");
      $(".inputSenha").addClass("inputSenhaErro");
      $(".inputSenhaC").addClass("inputSenhaCErro");
      Erro++;
    }
  }

  if (!termos) {
    $(".campoTermos").css("display", "block");
    Erro++;
  }
  if (Erro == 0) {
    $('#form-registro').submit();
  }
}

$(document).ready(function () {
  // VALIDAÇÃO DO BACKEND
  var url = window.location.href;
  var param1 = url.split("?")[1];
  var param2 = url.split("?")[2];
  var param3 = url.split("?")[3];

  if (typeof param2 === "undefined") {
  } else {
    var nome = atob(param2);
    $(".inputNome").val(decodeURIComponent(escape(nome)));
  }
  if (typeof param3 === "undefined") {
  } else {
    var email = atob(param3);
    $(".inputEmail").val(email);
  }
  if (typeof param1 === "undefined") {
    // console.log('safe');
  } else {
    var decodeErro = atob(param1);
    switch (decodeErro) {
      case "campoNomeEmailSenhaSenhaC":
      $(".campoNome").css("display", "block");
      $(".inputNome").addClass("inputNomeErro");

      $(".campoEmail").css("display", "block");
      $(".inputEmail").addClass("inputEmailErro");

      $(".campoSenha").css("display", "block");
      $(".inputSenha").addClass("inputSenhaErro");

      $(".campoSenhaC").css("display", "block");
      $(".inputSenhaC").addClass("inputSenhaCErro");
      break;
      case "campoNome":
      $(".campoNome").css("display", "block");
      $(".inputNome").addClass("inputNomeErro");
      break;
      case "campoEmail":
      $(".campoEmail").css("display", "block");
      $(".inputEmail").addClass("inputEmailErro");
      break;
      case "campoSenha":
      $(".campoSenha").css("display", "block");
      $(".inputSenha").addClass("inputSenhaErro");
      break;
      case "campoSenhaC":
      $(".campoSenhaC").css("display", "block");
      $(".inputSenhaC").addClass("inputSenhaCErro");
      break;
      case "campoSenhaDiff":
      $(".campoSenhaDiff").css("display", "block");
      $(".inputSenhaC").addClass("inputSenhaCErro");
      $(".inputSenha").addClass("inputSenhaErro");
      break;
      case "campoTermos":
      $(".campoTermos").css("display", "block");
      break;

      case "registroErro":
      swal.fire("Ops!", "Erro ao efetuar o cadastro. Por favor, tente novamente!", "error");
      break;
      case "emailCadastrado":
      swal.fire("Ops!", "E-mail já cadastrado. Por favor, tente redefinir sua senha!", "error");
      break;
      default:
      swal.fire("Deu Ruim!", "Algum erro inesperado, entre em contato com o programador mais próximo.", "error");
    }
  }
});
