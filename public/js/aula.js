$(document).ready(function() {

  if(window.screen.availWidth > 400){
    $("#conteudo").css({"-moz-column-count":"2","-webkit-column-count":"2","-o-column-rule":"1px dotted #006d9a","-webkit-column-rule":"1px dotted #006d9a"});
  }
});

var fraseOuca
var _ouca =  true;

//leituta de texto, api usada https://developer.mozilla.org/en-US/docs/Web/API/SpeechSynthesis

var count = 0;
var texto = "";
function PlayOuca() {
  speechSynthesis.cancel();
  recognition.stop();

  var texto = String(window.getSelection())



  if(texto.length > 0 ){
    swal({
      title: "Você está ouvindo!",
      text: texto,
      showCancelButton: false,
      confirmButtonText: "Ouvir Novamente",
    }).then(function() {
      speechSynthesis.cancel();
      PlayOuca()
    }, function(b) {
      if (b === "cancel") {}
    });
    msg = new SpeechSynthesisUtterance();
    msg.text = texto;
    msg.volume = 1;
    msg.rate = 0.6;
    msg.pitch = 1;
    msg.voice = speechSynthesis.getVoices().filter(function(b) {
      return b.name == idioma
    })[0];
    window.speechSynthesis.speak(msg)
  }

}



//janela de manual do aluno em relação ao recursos, pegando exatamente o recurso que o aluno esta, a função é chamada por um onmouseover no link Manual do aluno
function ManualAluno() {
  switch (aula_3) {
    case 1:

    swal({
      title: 'Listen',
      html: "<div style='overflow: scroll; height: 430px; width: 770px; overflow-x: hidden;'><p translate='no'><img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/listen_manual.gif' alt='' width='566' height='270' /></p><p style='text-align: justify;'>Aqui voc&ecirc; <strong>selecionar&aacute; </strong>a frase que deseja ouvir e dever&aacute; prestar aten&ccedil;&atilde;o na pron&uacute;ncia que ouvir&aacute; com base na sua sele&ccedil;&atilde;o. Fa&ccedil;a isso at&eacute; concluir todo texto. Se voc&ecirc; optar ouvir <strong>todo</strong> o texto e n&atilde;o frase por frase, e, supostamente deseje interromp&ecirc;-lo, poder&aacute; fazer isso a qualquer momento, basta clicar em qualquer espa&ccedil;o da tela. Assim, ficar&aacute; <strong>a seu crit&eacute;rio</strong> como deseja ouvir.</p><p style='text-align: justify;'>Em seguida, voc&ecirc; poder&aacute; visualizar a tradu&ccedil;&atilde;o de tudo que ouviu, clique em <img style='display: block; margin-left: auto; margin-right: auto;' src='https://efidiomas.com/biblioteca/traducao-certinha.png' alt='' width='80' height='40' /></p></div>",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Ok',
      cancelButtonText: 'Sobre o Manual',
      width: 800,
      allowOutsideClick: false
    }).then(function () {
      onClose();
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: "Manual",
          html:'O manual',
          width: 800,
          allowOutsideClick: false
        });
      }
    })
    break;
    case 2:

    swal({
      title: 'Talk',
      html: "<div style='overflow: scroll; height: 430px; width: 770px; overflow-x: hidden;'><p translate='no'><img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/talk_manual.gif' alt='' width='566' height='270' /></p><p style='text-align: justify;'>&nbsp;</p><p style='text-align: justify;'>Neste recurso voc&ecirc; precisar&aacute; clicar sobre o microfone <img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/microfone.png' alt='' width='30' height='30' /> Voc&ecirc; dever&aacute; pronunciar a frase sugerida, se a pron&uacute;ncia estiver correta voc&ecirc; poder&aacute; clicar na frase seguinte e assim sucessivamente, at&eacute; concluir. Clique em (&iacute;cone) para ouvir a pron&uacute;ncia correta.</p></div>",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Ok',
      cancelButtonText: 'Sobre o Manual',
      width: 800,
      allowOutsideClick: false
    }).then(function () {
      onClose();
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: "Manual",
          html:'O manual',
          width: 800,
          allowOutsideClick: false
        });
      }
    })
    break;
    case 3:
    swal({
      title: 'Read',
      html: "<div style='overflow: scroll; height: 430px; width: 770px; overflow-x: hidden;'><p translate='no'><img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/read_manual.gif' alt='' width='566' height='270' /></p><p style='text-align: justify;'>&nbsp;</p><p style='text-align: justify;'>Você deverá ler o texto proposto. Mesmo que apresente dificuldade em pronunciar alguma palavra, continue até concluir. Após, selecione todo o texto para ouvir a pronúncia correta e clique em “tradução” para compreender a leitura.</p></div>",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Ok',
      cancelButtonText: 'Sobre o Manual',
      width: 800,
      allowOutsideClick: false
    }).then(function () {
      onClose();
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: "Manual",
          html:'O manual',
          width: 800,
          allowOutsideClick: false
        });
      }
    })
    break;
    case 4:
    swal({
      title: 'Grammar',
      html:"<div style='overflow: scroll; height: 430px; width: 770px; overflow-x: hidden;'><img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/grammar_manual.gif' alt='' width='566' height='270' /></p><p style='text-align: justify;'>&nbsp;</p><p style='text-align: justify;'>Neste recurso você aprenderá sobre o conjunto de normas e regras que determinam o uso considerado correto da língua escrita e falada.</p></div>",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Ok',
      cancelButtonText: 'Sobre o Manual',
      width: 800,
      allowOutsideClick: false
    }).then(function () {
      onClose();
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: "Manual",
          html:'O manual',
          width: 800,
          allowOutsideClick: false
        });
      }
    })
    break;
    case 5:
    swal({
      title: 'Write',
      html:"<div style='overflow: scroll; height: 430px; width: 770px; overflow-x: hidden;'><p translate='no'><img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/write_manual.gif' alt='' width='566' height='270' /></p><p style='text-align: justify;'>Neste recurso, voc&ecirc; encontrar&aacute; em cada frase uma palavra entre par&ecirc;nteses, que dever&aacute; escrever em ingl&ecirc;s para completar a frase. Escreva sob o tra&ccedil;o apenas a palavra entre par&ecirc;nteses.</p><p style='text-align: justify;'>&nbsp;Ap&oacute;s escrever todas as palavras de cada frase, clique em <input id='verificar' style='background-color:#006d9a; border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF;' type='submit' value='Verificar'>.</p></div>",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Ok',
      cancelButtonText: 'Sobre o Manual',
      width: 800,
      allowOutsideClick: false
    }).then(function () {
      onClose();
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: "Manual",
          html:'O manual',
          width: 800,
          allowOutsideClick: false
        });
      }
    })
    break;
    case 6:
    swal({
      title: 'Media',
      html: "<div style='overflow: scroll; height: 430px; width: 770px; overflow-x: hidden;'><p translate='no'><img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/media_manual.gif' alt='' width='566' height='270' /></p><p style='text-align: justify;'>Caro aluno, clique no v&iacute;deo e perceba a intera&ccedil;&atilde;o entre o conte&uacute;do visto e as curiosidades superinteressantes a respeito dele.</p></div>",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Ok',
      cancelButtonText: 'Sobre o Manual',
      width: 800,
      allowOutsideClick: false
    }).then(function () {
      onClose();
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: "Manual",
          html:'O manual',
          width: 800,
          allowOutsideClick: false
        });
      }
    })
    break;
    case 7:
    swal({
      title: 'Vocabulary',
      html:"<div style='overflow: scroll; height: 430px; width: 770px; overflow-x: hidden;'><img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/vocabulary_manual.gif' alt='' width='566' height='270' /></p><p style='text-align: justify;' translate='no'>Aqui voc&ecirc; perceber&aacute; a sele&ccedil;&atilde;o de palavras da l&iacute;ngua inglesa referente ao cap&iacute;tulo e suas classifica&ccedil;&otilde;es: verbos, substantivos, adjetivos, entre outros.</p></div>",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Ok',
      cancelButtonText: 'Sobre o Manual',
      width: 800,
      allowOutsideClick: false
    }).then(function () {
      onClose();
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: "Manual",
          html:'O manual',
          width: 800,
          allowOutsideClick: false
        });
      }
    })
    break;
    case 8:
    swal({
      title: 'Know More',
      html:"<div style='overflow: scroll; height: 430px; width: 770px; overflow-x: hidden;'><img style='display: block; margin-left: auto; margin-right: auto;' src='/assets/imagens/knowmore_manual.gif' alt='' width='566' height='270' /></p><p style='text-align: justify;'>Esse recurso permite que voc&ecirc; complemente seus estudos com conte&uacute;dos din&acirc;micos e curiosos! Explore com aten&ccedil;&atilde;o e se surpreenda com as informa&ccedil;&otilde;es que far&atilde;o voc&ecirc; aprender ainda mais!</p></div>",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Ok',
      cancelButtonText: 'Sobre o Manual',
      width: 800,
      allowOutsideClick: false
    }).then(function () {
      onClose();
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: "Manual",
          html:'O manual',
          width: 800,
          allowOutsideClick: false
        });
      }
    })
    break;
  }
}


//função recurso escreva
function form_escreva() {

  //declarando todas as variaveis necessárias
  var cont = 0;
  var resposta_aluno;
  var valor_post;
  var val = $("#formulario_escreva").find("input").size();
  var quan_frases = val - 1;
  var contar = 0;
  var acertos;
  var html_escreva;
  var count;
  var campo_vazio = 0;
  var clicou = false;
  var errou = false;
  var quan_comparar = quan_frases - 1; //é subtraido a quantidade de inputs por -1 pois ao contar ele conta o botao verificar;


  //unindo todas as respostas em apenas uma string, exemplo: ta*ta*ta* separando por asteriscos, caso acha campo em branco a verificação é cancelada
  for (i = 1; i <= quan_frases; i++) {
    if (i == 1) {
      resposta_aluno = $("input[type=text][name=" + i + "]").val();
      $("input[type=text][name=" + i + "]").text(resposta_aluno);
      if ($("input[type=text][name=" + i + "]").val() == "") {
        campo_vazio = 1
      }
    } else {
      resposta_aluno = resposta_aluno + $("input[type=text][name=" + i + "]").val();
      $("input[type=text][name=" + i + "]").text(resposta_aluno);
      if ($("input[type=text][name=" + i + "]").val() == "") {
        campo_vazio = 1
      }
    }
    if (i < quan_frases) {
      resposta_aluno = resposta_aluno + "*"
    }
  }

  //copiando o html da pagina, pois caso o aluno acerte todas as respostas,quando ele entra novamente na lição esse html carregará sem a opção de responder novamente
  html_escreva = document.getElementById("escreva").innerHTML;
  //alerta de verificando resposta enquanto é feito um post para verificação
  swal({
    title: "Aguarde",
    text: "Verificando as respostas",
    type: "info",
    showConfirmButton: false,
  });
  $.post("/exercicios/escreva", {
    resposta: "" + resposta_aluno.toLowerCase() + "",
    posicao: "" + quan_frases + "",
    html: "" + html_escreva + "",
    campo_vazio: "" + campo_vazio + "",
    Id_Texto: aula_2,
    Id_Curso: id_curso_info
  }, function(data) {
    if(data == "sem respostas"){
      swal(
        'Ops!',
        'Não há respostas cadastradas!',
        'warning'
      )
    }

    //retornará o valor 1 para resposta certa e 0 para errada, foi usado o metodo split para separa em uma array.
    acertos = data.split("");
    if(data != "campo_vazio" && data != "sem respostas" ){
      for(i=1;i<=quan_frases;i++){
        //verifica cada posicao do vetor se a algum valor 0, caso tenha é apagado a resposta no input e setando errou como true
        if(acertos[contar]==1){
          cont++;
          if(cont == quan_frases){
            if(errou == false){
              location.reload();
            }
          }
        }else{
          $("input[type=text][name=" + i + "]").val("");
          $("input[type=text][name=" + i + "]").css({
            border: "none",
            "border-bottom": "2px solid red"
          });

          swal(
            'Ops!',
            'Os Campos em branco estão incorretos, tente novamente, lembre-se de ficar atento sobre as pontuações,letras maiúsculas e minúsculas!  ',
            'error'
          )
          errou = true;
        }
        contar = contar + 1

      }
    }
    if(data == "campo_vazio"){
      swal(
        'Ops!',
        'Por Favor, responda todas as questões antes de verificar!',
        'warning'
      )
      campo_vazio = 0;
    }
  }
);
}

//recurso fale
var cont = 1;
var frase;
var recognition = new webkitSpeechRecognition();

function falar() {

  var b;
  var contarFrases = $("#fale").find("input").size();
  if (window.hasOwnProperty("webkitSpeechRecognition")) {
    recognition.continuous = false;
    recognition.interimResults = false;
    recognition.lang = idioma_fala;
    recognition.start();
    frase = $("input[type=text][name=" + cont + "]").val();
    swal({
      title: "Fale a frase:",
      html: '<br><img src="/assets/imagens/ler_index.png" onclick="ler_index()" width="25" height="25" alt=""/> ' + frase,
      showConfirmButton: false,
      imageUrl: "http://www.ocolinense.com.br/imgs/carregando.gif",
    });
    recognition.onresult = function(e) {
      var fraseE = frase.replace("?", "");
      fraseE = fraseE.replace(",", "");
      fraseE = fraseE.replace(".", "");
      fraseE = fraseE.replace("'", "");
      fraseE = fraseE.replace("", "");
      fraseE = fraseE.replace("¿","")
      fraseE = fraseE.replace("?","")
      fraseE = fraseE.replace("!","")
      fraseE = fraseE.replace("¡","")

      var fraseEditada = fraseE.toLowerCase();

      var falouEditado = e.results[0][0].transcript.toLowerCase();
      falouEditado = falouEditado.replace("?", "");
      falouEditado = falouEditado.replace(",", "");
      falouEditado = falouEditado.replace(".", "");
      falouEditado = falouEditado.replace("'", "");
      falouEditado = falouEditado.replace("¿","")
      falouEditado = falouEditado.replace("?","")
      falouEditado = falouEditado.replace("!","")
      falouEditado = falouEditado.replace("¡","")

      if (falouEditado == fraseEditada) {
        recognition.stop();
        swal({
          title: "Pronúncia correta, pressione o microfone e fale a próxima frase",
          html: '<img src="http://i.imgur.com/cHidSVu.gif" onclick="falar()" />',
          showConfirmButton: false,
        });
        if (cont >= contarFrases) {
          swal({
            title: "Parabens!",
            text: "Exercicio concluido com sucesso!",
            type: "success",
            allowOutsideClick: false,
            showConfirmButton: false,
          });
          var htmlFale = document.getElementById("fale").innerHTML;
          $.post("/exercicios/fale", {
            html_fale: "" + htmlFale + "",
            Id_Curso: id_curso_info,
            Id_Texto: aula_2
          }, function(e) {
            swal({
              title: "Aguarde",
              text: "Salvando progresso!",
              type: "info",
              showConfirmButton: false,
            });
            location.reload()
          })
        } else {
          cont++
        }
      } else {
        recognition.stop();
        swal({
          title: "pronúncia incorreta, você disse <br>" + falouEditado + "<br>pressione o microfone e fale novamente",
          html: '<img src="http://i.imgur.com/cHidSVu.gif" onclick="falar()" />',
          showConfirmButton: false,
        })
      }
    }
  } else {
    swal("ups", "Para o perfeito funcionamento do recurso, use o navegador chrome", "error")
  }
}


function ler_index() {
  speechSynthesis.cancel();
  recognition.stop();
  if (window.getSelection) {
    var texto = String(window.getSelection())
  }
  swal({
    title: "Você está ouvindo!",
    text: frase,
    showCancelButton: false,
    confirmButtonText: "Falar",
  }).then(function() {
    speechSynthesis.cancel();
    falar()
  }, function(b) {
    if (b === "cancel") {}
  });
  msg = new SpeechSynthesisUtterance();
  msg.text = frase;
  msg.volume = 1;
  msg.rate = 0.6;
  msg.pitch = 1;
  msg.voice = speechSynthesis.getVoices().filter(function(b) {
    return b.name == idioma
  })[0];
  window.speechSynthesis.speak(msg)
};
