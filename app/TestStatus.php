<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestStatus extends Model
{
    protected $table = 'tb_TestStatus';
    protected $fillable = ['id_Usuario','id_test','done','pts','startDate','finishDate','Questoes_Sorteadas','userSolution'];
}
