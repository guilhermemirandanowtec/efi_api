<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespostasExtras extends Model
{
    protected $table = 'tb_respostas_extras';

    protected $fillable = ['id_questao', 'id_usuario', 'resposta_usuario', 'status'];

    public $timestamps = false;
}
