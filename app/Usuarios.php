<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model{
    protected $table = 'tb_Usuarios';
    protected $primaryKey = 'Id_Usuario';

    protected $fillable = ['Nome',
                          'TelCelular',
                          'Endereco',
                          'Numero',
                          'Bairro',
                          'Cidade',
                          'Estado',
                          'Complemento',
                          'CEP',
                          'Senha'];

    public function getDados($usuario){

      $sql = Usuarios::find($usuario);

      return $sql;
    }
}
