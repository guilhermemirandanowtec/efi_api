<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{
    protected $table = 'tb_Cursos';
    protected $primaryKey = 'Id_Curso';
}
