<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progresso extends Model
{
    protected $table = 'tb_Progresso';
    protected $primaryKey = 'Id';

    protected $fillable = ['Aula', 'Exercicio'];

    public $timestamps = false;
}
