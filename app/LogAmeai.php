<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAmeai extends Model
{
    protected $table = 'tb_LogAmea';

    protected $fillable = ['Id_Usuario','Data','Hora','Pagina','Ip','Id_Curso'];
    
    public $timestamps = false;
}
