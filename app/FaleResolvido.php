<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaleResolvido extends Model
{
    protected $table = 'tb_faleresolvido';
    protected $fillable = ['id_usuario', 'id_texto', 'nota', 'texto'];

    public $timestamps = false;
}
