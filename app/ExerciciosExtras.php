<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RespostasExtras;
use Carbon\Carbon;

class ExerciciosExtras extends Model
{
    protected $table = 'tb_exercicios_extras';

    public static function salva_tentativa($id_questao, $resposta_usuario, $id_usuario){

       $status = ExerciciosExtras::verifica_resposta($id_questao, $resposta_usuario);

       RespostasExtras::create([
        'id_questao' => $id_questao,
        'id_usuario' => $id_usuario,
        'resposta_usuario' => $resposta_usuario,
        'status' => $status,
        'created_at' => Carbon::now()
       ]);

    }

    public static function verifica_resposta($id_questao, $resposta_usuario){

      $resposta = ExerciciosExtras::where('id',$id_questao)
      ->where('alternativa_a', $resposta_usuario)
      ->count();

      if($resposta <= 0){
        return "errada";
      }
      else{
        return "correta";
      }
    }
}
