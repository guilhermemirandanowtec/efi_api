<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EscrevaResolvido extends Model
{
    protected $table = 'tb_escrevaresolvido';
    protected $fillable = ['id_usuario', 'id_texto', 'nota', 'texto'];

    public $timestamps = false;
}
