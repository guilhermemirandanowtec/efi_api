<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAcessoAMEAI extends Model
{
    protected $table = 'tb_logAcessoAMEAI';
    protected $fillable = ['id_usuario', 'id_curso', 'id_capitulo', 'tab', 'hora_inicio', 'hora_final'];
    protected $primaryKey = 'id_logAcessoAMEAI';

    public $timestamps = false;
}
