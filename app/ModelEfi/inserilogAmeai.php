<?php

namespace App\ModelEfi;

use Illuminate\Database\Eloquent\Model;
use App\LogAmeai;

class inserilogAmeai extends Model{

	public function inseriLog($Id_Curso, $Aula, $id_aluno){

		$aula = $Aula + 1;
		$ip = $_SERVER['REMOTE_ADDR'];
		date_default_timezone_set('America/Sao_Paulo');
		$data = date ("Y-m-d",time());
		$hora = date("H:i:s",time());

		LogAmeai::create([
			'Id_Usuario' => $id_aluno,
			'Data' => $data,
			'Hora' => $hora,
			'Pagina' => $aula,
			'Ip' => $ip,
			'Id_Curso' => $Id_Curso,
		]);
	}
}
