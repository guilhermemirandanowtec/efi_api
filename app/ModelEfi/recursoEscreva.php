<?php
class recursoEscreva extends model{

		public function verificaEscrevaFinalizado($id_texto,$id_usuario){
			$sql = "SELECT * FROM tb_escrevaresolvido WHERE id_usuario = $id_usuario AND id_texto = $id_texto";
			$sql = $this->db->query($sql);
			$retorno = $sql->fetchAll();

			if(isset($retorno[0]['texto'])){
				$retorno[0]['texto'] = $retorno[0]['texto'];
			}

			return $retorno;
		}

    public function salvarEscreva($id_texto, $texto, $id_curso, $nota)
    {
				$nota = round($nota);
        $id_usuario =  $_SESSION['id_usuario'];
        $sql = "INSERT INTO tb_escrevaresolvido (texto,id_usuario,id_texto,nota) VALUES (?, ?, ?, ?)";
        $sql = $this->db->prepare($sql);
				$sql->bindParam(1, $texto);
				$sql->bindParam(2, $id_usuario);
				$sql->bindParam(3, $id_texto);
				$sql->bindParam(4, $nota);
        $sql->execute();

        $ListarExercicios = "SELECT * FROM tb_Progresso WHERE Id_Aluno = $id_usuario AND Id_Curso = $id_curso;";
        $ListarExercicios = $this->db->query($ListarExercicios);
        $dadosExercicios = $ListarExercicios->fetchAll();
        $exercicio = $dadosExercicios[0]['Exercicio'];
        $exercicio++;
        $aula = $dadosExercicios[0]['Aula'];

				if($exercicio % 2 == 0){
					$aula++;
	        $sql = "UPDATE tb_Progresso
									SET Exercicio = $exercicio, Aula = $aula
									WHERE Id_Aluno = $id_usuario
									AND Id_Curso = $id_curso";
				}else{
					$sql = "UPDATE tb_Progresso
									SET Exercicio = $exercicio, Aula = $aula
									WHERE Id_Aluno = $id_usuario
									AND Id_Curso = $id_curso";
				}
        $sql = $this->db->prepare($sql);
        $sql->execute();

        return $sql;
    }

    public function finalizarEscreva($respostas, $id_texto)
    {
        $sql = "SELECT * FROM tb_textos WHERE Id_texto = $id_texto AND Excluido <> '1'";
        $textoAtual = $this->db->query($sql);
        $textoAtual = $textoAtual->fetchAll();

        $retorno['texto'] = $textoAtual[0]['texto'];
        $retorno['id_curso'] = $textoAtual[0]['Id_Curso'];
        $retorno = json_encode($retorno);

        return $retorno;
    }

    public function verificaEscreva($respostas, $id_texto, $id_curso)
    {
        $sql = "SELECT * FROM tb_Respostas_Recursos WHERE Id_texto = $id_texto AND Excluido <> '1'";
        $respostasCorretas = $this->db->query($sql);
        $dadosRespostas = $respostasCorretas->fetchAll();
        $respostasCorretas = explode('*', $dadosRespostas[0]['Resposta']);
        $numAcertos = 0;
        $acertos = "";

        for ($i=0; $i < sizeof($respostasCorretas); $i++) {
            $respostasCorretas[$i] = trim($respostasCorretas[$i]);
            if (isset($respostas[$i+1]) && strtolower($respostasCorretas[$i]) == strtolower($respostas[$i+1])) {
                $numAcertos++;
                $acertos = $acertos.'1';
            } else {
                $acertos = $acertos.'0';
            }
        }

        $retorno['numAcertos'] = $numAcertos;
        $retorno['acertos'] = $acertos;
        return $retorno;
    }

    public function verificaEscrevas($r_usuario, $posicao, $html, $campo_vazio, $quantidade_perguntas, $id_texto, $Id_Curso)
    {
        $html = str_replace("\"", "'", $html);
        $cont = 0;
        $acertos = "";
        $acertos_sessao = 0;
        $certas = 0;
        $Id_Aluno =  $_SESSION['id_usuario'];
        if ($campo_vazio == 1) {
            echo'campo_vazio';
        } elseif ($campo_vazio == 0) {
            $respostas = "SELECT * FROM tb_Respostas_Recursos WHERE Id_texto = $id_texto AND Excluido <> '1'";
            $respostas = $this->db->query($respostas);
            $dadosRespostas = $respostas->fetchAll();
            $certas = explode("*", $dadosRespostas[0]['Resposta']);

            for ($i=0;$i<=$posicao;$i++) {
                $cont++;
                $html = str_replace("name='".$cont."'", "name='".$cont."' value='".$r_usuario[$i]."' style='color:green;font-size: 16px;background-color:white;' disabled", $html);
                $certas[$i] = trim($certas[$i]);
                $r_usuario[$i] = trim($r_usuario[$i]);

                if (strtolower($certas[$i]) == $r_usuario[$i]) {
                    $valor = 1;
                    $acertos = $acertos."".$valor;
                    $acertos_sessao++;
                } else {
                    $valor = 0;
                    $acertos = $acertos."".$valor;
                }
            }

            //$html = str_replace("<input id='verificar' onclick='form_escreva()' style='background-color:#006d9a; border-width: 0px; padding: 5px  10px;  font-size: 16px; color: #FFF;' type='submit' value='Verificar'>","<img src='".BASE_URL."/assets/imagens/exercicio_concluido.png'>",$html);

            if ($acertos_sessao == $quantidade_perguntas) {
                $ListarExercicios = "SELECT * FROM tb_Progresso WHERE Id_Aluno = $Id_Aluno AND Id_Curso = $Id_Curso;";
                $ListarExercicios = $this->db->query($ListarExercicios);
                $dadosExercicios = $ListarExercicios->fetchAll();
                $exercicio = $dadosExercicios[0]['Exercicio'];
                $exercicio++;
                $aula = $dadosExercicios[0]['Aula'];

                if ($exercicio % 2 == 0 && $exercicio != 0) {
                    $aula++;
                    $UpdateExercicio = "UPDATE tb_Progresso SET Exercicio = ? , Aula = ? WHERE Id_Aluno = ? and Id_Curso = ?";
                    $UpdateExercicio = $this->db->prepare($UpdateExercicio);
                    $UpdateExercicio->bindParam(1, $exercicio);
                    $UpdateExercicio->bindParam(2, $aula);
                    $UpdateExercicio->bindParam(3, $Id_Aluno);
                    $UpdateExercicio->bindParam(4, $Id_Curso);
                    $UpdateExercicio->execute();
                } else {
                    $UpdateExercicio = "UPDATE tb_Progresso SET Exercicio = ? WHERE Id_Aluno = ? and Id_Curso = ?";
                    $UpdateExercicio = $this->db->prepare($UpdateExercicio);
                    $UpdateExercicio->bindParam(1, $exercicio);
                    $UpdateExercicio->bindParam(2, $Id_Aluno);
                    $UpdateExercicio->bindParam(3, $Id_Curso);
                    $UpdateExercicio->execute();
                }
                $RecursosInseri = "INSERT INTO tb_Recursos_Resolvidos(Id_Aluno,Id_Texto,Html) VALUES (?, ?, ?)";
                $RecursosInseri = $this->db->prepare($RecursosInseri);
                $RecursosInseri->bindParam(1, $Id_Aluno);
                $RecursosInseri->bindParam(2, $id_texto);
                $RecursosInseri->bindParam(3, $html);
                $RecursosInseri->execute();
            }
            return $acertos;
        }
    }
}
