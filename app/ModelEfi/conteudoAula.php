<?php

namespace App\ModelEfi;

use Illuminate\Database\Eloquent\Model;
use App\Matriculas;
use App\Progresso;
use App\Texto;
use App\Cursos;
use App\RecursosResolvidos;

class conteudoAula extends Model{


	public function getSumarioBkp($Curso){

		$_idioma;
		$licoes = array();
		$cont = 0;
		$ultimoCurso = "";

		$Idioma = "Select * from tb_Cursos Where Id_Curso = $Curso";
		$Idioma = $this->db->query($Idioma);
		$Idioma = $Idioma->fetchAll();
		$idioma = $Idioma[0]['Idioma'];

		$Modulo = "Select * from tb_Cursos inner join tb_Capitulos on (tb_Cursos.Id_Curso = tb_Capitulos.Id_Curso) where tb_Cursos.Id_Curso = $Curso and tb_Capitulos.Excluido <> 1 Order by tb_Capitulos.Ordem";
		$Modulo = $this->db->query($Modulo);
		if(!$Modulo)
		{
			die("Execute query error, because: ". print_r($this->db->errorInfo(),true) );
		}else{
			$dadosModulos = $Modulo->fetchAll();
		}
		return  $dadosModulos;
	}

	public function getSumario($Curso){

		$Modulo = Cursos::join('tb_Capitulos', 'tb_Cursos.Id_Curso', 'tb_Capitulos.Id_Curso')
		->where('tb_Cursos.Id_Curso', $Curso)
		->where('tb_Capitulos.Excluido', '<>', 1)
		->orderBy('tb_Capitulos.Ordem')
		->get();

		return $Modulo;
	}

	public function getConteudoBkp($curso,$ordem,$aba){

		$dados =  array();
		$idioma = array();
		$aluno = $_SESSION['id_usuario'] ;
		$retorno = array();
		$UltimaAulaFeita;


		$verificaMatricula = "Select * from tb_Matriculas where Id_Curso = $curso and Id_Usuario = $aluno";
		$verificaMatricula = $this->db->query($verificaMatricula);
		$dadosverificamatricula = $verificaMatricula->fetchAll();

		if($verificaMatricula->rowCount() > 0){
			if($aba == 1){
				$Recurso = 'ouca';
			}else if($aba == 2){
				$Recurso = 'fale';
			}else if($aba == 3){
				$Recurso = 'leia';
			}else if($aba == 4){
				$Recurso = 'gramatica';
			}else if($aba == 5){
				$Recurso = 'escreva';
			}else if($aba == 6){
				$Recurso = 'video';
			}else if($aba == 7){
				$Recurso = 'vocabulario';
			}else if($aba == 8){
				$Recurso = 'saiba mais';
			}
			$UltimaAula = "Select * From tb_Progresso where Id_Curso = $curso and Id_Aluno = $aluno";
			$UltimaAula = $this->db->query($UltimaAula);
			$dadosAulaFeita = $UltimaAula->fetchAll();
			$UltimaAulaFeita = $dadosAulaFeita[0]['Aula'];

			if($UltimaAulaFeita < $ordem){
				?>
            <script>alert("Aprendiz para prosseguir para próxima aula conclua os recursos fale e escreva da aula atual!")</script>
            <?php
                $aulaAtual = $ordem - 1;
                header('Location:' . BASE_URL . '/ameai/aula/' . $curso . '/' . $aulaAtual .'/1');
				exit();
			}else{
				$sql = "SELECT * FROM tb_textos INNER JOIN tb_Capitulos ON (tb_textos.Id_Capitulo = tb_Capitulos.Id_Capitulo) INNER JOIN tb_Licao ON (tb_textos.Id_Licao = tb_Licao.Id_Licao) INNER JOIN tb_Cursos ON (tb_textos.Id_Curso = tb_Cursos.Id_Curso) where Tipo_Licao = '$Recurso' and tb_textos.Id_Curso= $curso and tb_Capitulos.Ordem= $ordem and tb_textos.Excluido <> 1";
				$sql = $this->db->query($sql);
				$dados  =  $sql->fetchAll();
				$Idioma = $dados[0]['Idioma'];
				$id_texto = $dados[0]['Id_texto'];


				if($sql->rowCount() > 0) {
					if($aba == 1){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("<button onclick='ler()'>Ou&ccedil;a</button>","",$valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("<button onclick='stop_leitura()'>Stop</button>","",$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="ouca"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);


						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);

						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

            $valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);

                     //resolução imagem botao
                     //$valor =   str_replace("width: 250px; height: 225px;","width: 100px; height: 113px;",$valor);


					}else if($aba == 2){
							$IdiomaFale= "SELECT * FROM tb_Cursos Where Id_Curso = $curso";
							$IdiomaFale = $this->db->query($IdiomaFale);
							$valor = $dados[0]['texto'];
							$valor = str_replace("http:","https:",$valor);
							$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
							$valor = str_replace("input","input readonly='readonly'",$valor);
							$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
							$valor = str_replace("height: 350px","height: 100%",$valor);
							$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
							$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
							$valor = str_replace("overflow: auto;","",$valor);
							$_SESSION['IdiomaFale'] = $dados[0]['Idioma_Fala'];
							$_SESSION['IdiomaLeia'] = $dados[0]['Idioma'];
							$valor = str_replace("column-count: ","",$valor);
							$valor = '<div id="fale"><p>'.$valor.'</p></div>';
							$valor = str_replace("src='https://i.imgur.com/cHidSVu.gif'","src='".BASE_URL."/assets/imagens/microfone.png' style='cursor:pointer'",$valor);

							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
							$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
							$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);
							$valor = str_replace("onclick=\"falar()\"", "onclick=\"falar()\" id=\"botao_falar\"", $valor);

							$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);
              $valor = str_replace("https://i.imgur.com/cHidSVu.gif","https://www.efidiomas.com.br/assets/imagens/microfone.png",$valor);
					}else if($aba == 3){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("fale()",$Idioma,$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="leia"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);

						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);


					}else if($aba == 4){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="gramatica"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);

						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);




					}else if($aba == 5){

							$valor = str_replace("\"", "'",$dados[0]['texto']);
							$valor = str_replace("http:","https:",$valor);
							$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
							$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
							$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
							$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
							$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
							$valor = str_replace("height: 350px","height: 100%",$valor);
							$valor = str_replace("overflow: auto;","",$valor);
							$valor = str_replace("<button onclick='limpar_escreva()'>Tentar novamente</button>","",$valor);
							$valor = str_replace("column-count: ","",$valor);
							$valor = '<div id="escreva"><p>'.$valor.'</p></div>';
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
							$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
							$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);
							$valor = str_replace("onclick=\"falar()\"", "onclick=\"falar()\" id=\"botao_falar\"", $valor);

							$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);
					}else if($aba == 6){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("id='video'","id='video' allowfullscreen='true'",$valor);
						$valor = '<div id="video"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);





					}else if($aba == 7){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="vocabulario"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);



					}else if($aba == 8){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="saiba_mais"><p>'.$valor.'</p></div>';
						$valor = str_replace("//efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/Dados_Usuario/imagens_sistema_antigo/",$valor);
						$valor = str_replace("https://www.efidiomas.com.br/painel/PaginaConteudoExtra", "".BASE_URL."/ameai/PaginaConteudoExtra", $valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);



					}
				}
				$retorno[0] = $valor;
				$retorno[1] = $dados[0]['Nome_Capitulo'];
				$retorno[2] = $id_texto;
				$retorno[3] = $aba;
				$retorno[4] = $dadosverificamatricula[0]['Primeiro_Acesso'];
				$retorno[5] = $ordem;
				$retorno[6] = $UltimaAulaFeita;
				return $retorno;
			}
		}else{
			header('Location:'.BASE_URL);
			exit();
		}
	}

	public function getConteudo($curso, $ordem, $aba, $aluno){


		$verificaMatricula = Matriculas::where('Id_Curso', $curso)->where('Id_Usuario', $aluno)->get();

		if($verificaMatricula->count() > 0){
			if($aba == 1){
				$Recurso = 'ouca';
			}else if($aba == 2){
				$Recurso = 'fale';
			}else if($aba == 3){
				$Recurso = 'leia';
			}else if($aba == 4){
				$Recurso = 'gramatica';
			}else if($aba == 5){
				$Recurso = 'escreva';
			}else if($aba == 6){
				$Recurso = 'video';
			}else if($aba == 7){
				$Recurso = 'vocabulario';
			}else if($aba == 8){
				$Recurso = 'saiba mais';
			}
			$UltimaAula = Progresso::where('Id_Curso', $curso)->where('Id_Aluno', $aluno)->get();
			$UltimaAulaFeita = $UltimaAula[0]['Aula'];

			if($UltimaAulaFeita < $ordem){
				/*?>
            <script>alert("Aprendiz para prosseguir para próxima aula conclua os recursos fale e escreva da aula atual!")</script>
            <?php
                $aulaAtual = $ordem - 1;
                header('Location:' . BASE_URL . '/ameai/aula/' . $curso . '/' . $aulaAtual .'/1');
				exit();*/

				// Retorna um alert com uma mensagem de advertencia

				dd('nao chega aqui');
			}
			else{

				$dados = Texto::join('tb_Capitulos','tb_textos.Id_Capitulo','tb_Capitulos.Id_Capitulo')
				->join('tb_Licao','tb_textos.Id_Licao', 'tb_Licao.Id_Licao')
				->join('tb_Cursos','tb_textos.Id_Curso', 'tb_Cursos.Id_Curso')
				->where('Tipo_Licao', $Recurso)
				->where('tb_textos.Id_Curso', $curso)
				->where('tb_Capitulos.Ordem', $ordem)
				->where('tb_textos.Excluido','<>',1)
				->get();

				// $sql = "SELECT * FROM tb_textos INNER JOIN tb_Capitulos ON (tb_textos.Id_Capitulo = tb_Capitulos.Id_Capitulo) INNER JOIN tb_Licao ON (tb_textos.Id_Licao = tb_Licao.Id_Licao) INNER JOIN tb_Cursos ON (tb_textos.Id_Curso = tb_Cursos.Id_Curso) where Tipo_Licao = '$Recurso' and tb_textos.Id_Curso= $curso and tb_Capitulos.Ordem= $ordem and tb_textos.Excluido <> 1";
				// $sql = $this->db->query($sql);
				// $dados  =  $sql->fetchAll();
				$Idioma = $dados[0]['Idioma'];
				$id_texto = $dados[0]['Id_texto'];


				if($dados->count() > 0) {
					if($aba == 1){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("<button onclick='ler()'>Ou&ccedil;a</button>","",$valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("<button onclick='stop_leitura()'>Stop</button>","",$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="ouca"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);


						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);

						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

            $valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);

                     //resolução imagem botao
                     //$valor =   str_replace("width: 250px; height: 225px;","width: 100px; height: 113px;",$valor);


					}

					else if($aba == 2){
							// $IdiomaFale= "SELECT * FROM tb_Cursos Where Id_Curso = $curso";
							// $IdiomaFale = $this->db->query($IdiomaFale);
							$IdiomaFale = Cursos::where('Id_Curso', $curso)->get();
							$valor = $dados[0]['texto'];
							$valor = str_replace("http:","https:",$valor);
							$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
							$valor = str_replace("input","input readonly='readonly'",$valor);
							$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
							$valor = str_replace("height: 350px","height: 100%",$valor);
							$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
							$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
							$valor = str_replace("overflow: auto;","",$valor);

							// $_SESSION['IdiomaFale'] = $dados[0]['Idioma_Fala'];
							// $_SESSION['IdiomaLeia'] = $dados[0]['Idioma'];
							$valor = str_replace("column-count: ","",$valor);
							$valor = '<div id="fale"><p>'.$valor.'</p></div>';
							$valor = str_replace("src='https://i.imgur.com/cHidSVu.gif'","src='/assets/imagens/microfone.png' style='cursor:pointer'",$valor);

							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
							$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
							$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);
							$valor = str_replace("onclick=\"falar()\"", "onclick=\"falar()\" id=\"botao_falar\"", $valor);

							$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);
              $valor = str_replace("https://i.imgur.com/cHidSVu.gif","https://www.efidiomas.com.br/assets/imagens/microfone.png",$valor);
					}
					else if($aba == 3){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("fale()",$Idioma,$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="leia"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);

						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);


					}
					else if($aba == 4){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="gramatica"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);

						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);




					}
					else if($aba == 5){

							$valor = str_replace("\"", "'",$dados[0]['texto']);
							$valor = str_replace("http:","https:",$valor);
							$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
							$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
							$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
							$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
							$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
							$valor = str_replace("height: 350px","height: 100%",$valor);
							$valor = str_replace("overflow: auto;","",$valor);
							$valor = str_replace("<button onclick='limpar_escreva()'>Tentar novamente</button>","",$valor);
							$valor = str_replace("column-count: ","",$valor);
							$valor = '<div id="escreva"><p>'.$valor.'</p></div>';
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
							$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
							$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
							$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);
							$valor = str_replace("onclick=\"falar()\"", "onclick=\"falar()\" id=\"botao_falar\"", $valor);

							$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);
					}
					else if($aba == 6){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("id='video'","id='video' allowfullscreen='true'",$valor);
						$valor = '<div id="video"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);





					}
					else if($aba == 7){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="vocabulario"><p>'.$valor.'</p></div>';
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);



					}
					else if($aba == 8){
						$valor = str_replace("\"", "'",$dados[0]['texto']);
						$valor = str_replace("http:","https:",$valor);
						$valor = str_replace("<div id='conteudo' style=' border: 1px solid;'>","<div id='conteudo'>",$valor);
						$valor = str_replace("<div id='conteudo'>","<div id='conteudo' onmousedown='StopOuca()' onmouseup='PlayOuca()' >",$valor);
						$valor = str_replace("<a","<a data-fancybox data-type='iframe'",$valor);
						$valor = str_replace("efidiomas.com/painel/imagens/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("efidiomas.com/biblioteca/", "www.efidiomas.com.br/assets/imagens/imagens_sistema_antigo/", $valor);
						$valor = str_replace("height: 350px","height: 100%",$valor);
						$valor = str_replace("overflow: auto;","",$valor);
						$valor = str_replace("column-count: ","",$valor);
						$valor = '<div id="saiba_mais"><p>'.$valor.'</p></div>';
						$valor = str_replace("//efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/Dados_Usuario/imagens_sistema_antigo/",$valor);
						$valor = str_replace("https://www.efidiomas.com.br/painel/PaginaConteudoExtra", "".BASE_URL."/ameai/PaginaConteudoExtra", $valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_3/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_3/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_4/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_4/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_5/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_5/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_6/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_6/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_33/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_33/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_34/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_34/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_38/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_38/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_51/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_51/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Redator_66/","https://efidiomas.com.br/assets/imagens/imagens_sistema_antigo/Redator_66/",$valor);
						$valor = str_replace("https://efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://www.efidiomas.com/ava/mais.asp?id=","https://www.efidiomas.com.br/ameai/PaginaConteudoExtra/",$valor);
						$valor = str_replace("https://efidiomas.com/painel/imagens/Dados_Usuario/","https://www.efidiomas.com.br/assets/imagens/curso/imagens_sistema_antigo/Dados_Usuario/",$valor);

						$valor = str_replace("painel/PaginaConteudoExtra","ameai/PaginaConteudoExtra",$valor);



					}
				}
				$retorno[0] = $valor;
				$retorno[1] = $dados[0]['Nome_Capitulo'];
				$retorno[2] = $id_texto;
				$retorno[3] = $aba;
				$retorno[4] = $verificaMatricula[0]['Primeiro_Acesso'];
				$retorno[5] = $ordem;
				$retorno[6] = $UltimaAulaFeita;
				return $retorno;
			}
		}
		else{
			abort(404);
		}
	}

	public function InfoRecursosBkp($curso,$ordem){
		$aluno = $_SESSION['id_usuario'] ;

		$recursoFale = "SELECT * FROM tb_textos INNER JOIN tb_Capitulos ON (tb_textos.Id_Capitulo = tb_Capitulos.Id_Capitulo) INNER JOIN tb_Licao ON (tb_textos.Id_Licao = tb_Licao.Id_Licao) INNER JOIN tb_Cursos ON (tb_textos.Id_Curso = tb_Cursos.Id_Curso) where Tipo_Licao = 'fale' and tb_textos.Id_Curso= $curso and tb_Capitulos.Ordem= $ordem and tb_textos.Excluido <> 1";
		$recursoFale = $this->db->query($recursoFale);
		$dadosFale  =  $recursoFale->fetchAll();
		$id_textoFale = $dadosFale[0]['Id_texto'];

		$recursofaleFeito= "SELECT * FROM tb_Recursos_Resolvidos WHERE Id_Aluno = $aluno and Id_Texto = $id_textoFale";
		$recursofaleFeito = $this->db->query($recursofaleFeito);
		$dadosfeitoFale  =  $recursofaleFeito->fetchAll();

		if($recursofaleFeito->rowCount() > 0){
			$retorno[0] = 1;
			$retorno['textoFale'] =  utf8_encode(utf8_decode($dadosfeitoFale[0]['Html']));
		}else{
			$retorno[0] = 0;
			$retorno['textoFale'] = 0;
		}

		$recursoEscreva = "SELECT * FROM tb_textos INNER JOIN tb_Capitulos ON (tb_textos.Id_Capitulo = tb_Capitulos.Id_Capitulo) INNER JOIN tb_Licao ON (tb_textos.Id_Licao = tb_Licao.Id_Licao) INNER JOIN tb_Cursos ON (tb_textos.Id_Curso = tb_Cursos.Id_Curso) where Tipo_Licao = 'escreva' and tb_textos.Id_Curso= $curso and tb_Capitulos.Ordem= $ordem and tb_textos.Excluido <> 1";
		$recursoEscreva = $this->db->query($recursoEscreva);
		$dadosEscreva  =  $recursoEscreva->fetchAll();
		$id_textoEscreva = $dadosEscreva[0]['Id_texto'];

		$recursoescrevaFeito= "SELECT * FROM tb_Recursos_Resolvidos WHERE Id_Aluno = $aluno and Id_Texto = $id_textoEscreva";
		$recursoescrevaFeito = $this->db->query($recursoescrevaFeito);
		$dadosfeitoescreva  =  $recursoescrevaFeito->fetchAll();

		if($recursoescrevaFeito->rowCount() > 0){
			$retorno[1] = 1;
			$retorno['textoEscreva'] = utf8_encode(utf8_decode($dadosfeitoescreva[0]['Html']));
		}else{
			$retorno[1] = 0;
			$retorno['textoEscreva'] = 0;
		}

		return $retorno;

	}

	public function InfoRecursos($curso, $ordem, $aluno){

		$dadosFale = Texto::join('tb_Capitulos', 'tb_textos.Id_Capitulo', 'tb_Capitulos.Id_Capitulo')
		->join('tb_Licao', 'tb_textos.Id_Licao', 'tb_Licao.Id_Licao')
		->join('tb_Cursos', 'tb_textos.Id_Curso', 'tb_Cursos.Id_Curso')
		->where('Tipo_Licao', 'fale')
		->where('tb_textos.Id_Curso', $curso)
		->where('tb_Capitulos.Ordem', $ordem)
		->where('tb_textos.Excluido', '<>', 1)
		->get();

		$id_textoFale = $dadosFale[0]['Id_texto'];

		$dadosfeitoFale = RecursosResolvidos::where('Id_Aluno', $aluno)
		->where('Id_Texto', $id_textoFale)
		->get();

		if($dadosfeitoFale->count() > 0){
			$retorno[0] = 1;
			$retorno['textoFale'] =  utf8_encode(utf8_decode($dadosfeitoFale[0]['Html']));
		}
		else{
			$retorno[0] = 0;
			$retorno['textoFale'] = 0;
		}

		$dadosEscreva = Texto::join('tb_Capitulos', 'tb_textos.Id_Capitulo', 'tb_Capitulos.Id_Capitulo')
		->join('tb_Licao', 'tb_textos.Id_Licao', 'tb_Licao.Id_Licao')
		->join('tb_Cursos', 'tb_textos.Id_Curso', 'tb_Cursos.Id_Curso')
		->where('Tipo_Licao', 'escreva')
		->where('tb_textos.Id_Curso', $curso)
		->where('tb_Capitulos.Ordem', $ordem)
		->where('tb_textos.Excluido', '<>', 1)
		->get();

		$id_textoEscreva = $dadosEscreva[0]['Id_texto'];

		$dadosfeitoescreva = RecursosResolvidos::where('Id_Aluno', $aluno)
		->where('Id_Texto', $id_textoEscreva)
		->get();

		if($dadosfeitoescreva->count() > 0){
			$retorno[1] = 1;
			$retorno['textoEscreva'] = utf8_encode(utf8_decode($dadosfeitoescreva[0]['Html']));
		}
		else{
			$retorno[1] = 0;
			$retorno['textoEscreva'] = 0;
		}

		return $retorno;

	}

	public function ImagemAba($Id_Curso){
    $Aba = "SELECT * FROM tb_AC where Id_Curso = '$Id_Curso' and Visivel = 1";
		$Aba = $this->db->query($Aba);
		$dados  =  $Aba->fetchAll();
		return $dados;

	}

	public function VerificaFeitoFale($curso, $ordem){

		$sql = "SELECT * FROM tb_textos
						INNER JOIN tb_Capitulos ON (tb_textos.Id_Capitulo = tb_Capitulos.Id_Capitulo)
						INNER JOIN tb_Licao ON (tb_textos.Id_Licao = tb_Licao.Id_Licao)
						INNER JOIN tb_Cursos ON (tb_textos.Id_Curso = tb_Cursos.Id_Curso)
						WHERE Tipo_Licao = 'fale'
						AND tb_textos.Id_Curso= $curso
						AND tb_Capitulos.Ordem= $ordem
						AND tb_textos.Excluido <> 1";

		$sql = $this->db->query($sql);
		$dados  =  $sql->fetchAll();
		$id_texto = $dados[0]['Id_texto'];


		$aluno = $_SESSION['id_usuario'] ;
      $faleFeito= "SELECT * FROM tb_faleresolvido
			WHERE id_usuario = $aluno
			AND id_texto = $id_texto";

			$faleFeito = $this->db->query($faleFeito);
			$dadosFeito  =  $faleFeito->fetchAll();
			if($faleFeito->rowCount() > 0){
                  $retorno = "1";
			}else{
                  $retorno = "0";
			}

  	return $retorno;
	}

	public function VerificaFeitoEscreva($curso, $ordem){

		$sql = "SELECT * FROM tb_textos
						INNER JOIN tb_Capitulos ON (tb_textos.Id_Capitulo = tb_Capitulos.Id_Capitulo)
						INNER JOIN tb_Licao ON (tb_textos.Id_Licao = tb_Licao.Id_Licao)
						INNER JOIN tb_Cursos ON (tb_textos.Id_Curso = tb_Cursos.Id_Curso)
						WHERE Tipo_Licao = 'escreva'
						AND tb_textos.Id_Curso= $curso
						AND tb_Capitulos.Ordem= $ordem
						AND tb_textos.Excluido <> 1";

				$sql = $this->db->query($sql);
				$dados  =  $sql->fetchAll();
				$id_texto = $dados[0]['Id_texto'];

				$aluno = $_SESSION['id_usuario'];
      	$escrevaFeito= "SELECT * FROM tb_Recursos_Resolvidos WHERE Id_Aluno = $aluno and Id_Texto = $id_texto";
				$escrevaFeito = $this->db->query($escrevaFeito);
				$dadosFeito  =  $escrevaFeito->fetchAll();
				if($escrevaFeito->rowCount() > 0){
        	$retorno = "1";
				}else{
        	$retorno = "0";
				}
  		return $retorno;
	}

	public function RespostaEscreva($curso, $ordem){

    	$sql = "SELECT * FROM tb_textos INNER JOIN tb_Capitulos ON (tb_textos.Id_Capitulo = tb_Capitulos.Id_Capitulo) INNER JOIN tb_Licao ON (tb_textos.Id_Licao = tb_Licao.Id_Licao) INNER JOIN tb_Cursos ON (tb_textos.Id_Curso = tb_Cursos.Id_Curso) where Tipo_Licao = 'escreva' and tb_textos.Id_Curso= $curso and tb_Capitulos.Ordem= $ordem and tb_textos.Excluido <> 1";
				$sql = $this->db->query($sql);
				$dados  =  $sql->fetchAll();
				$id_texto = $dados[0]['Id_texto'];

      $respostaEscreva= "SELECT * FROM tb_Respostas_Recursos WHERE Id_Texto = $id_texto";
						$respostaEscreva = $this->db->query($respostaEscreva);
						$Respostas  =  $respostaEscreva->fetchAll();
						return $Respostas;


	}


}
