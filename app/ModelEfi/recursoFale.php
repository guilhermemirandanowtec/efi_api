<?php
class recursoFale extends model{

	public function verificaFaleFinalizado($id_texto,$id_usuario){
		$sql = "SELECT * FROM tb_faleresolvido WHERE id_usuario = $id_usuario AND id_texto = $id_texto";
		$sql = $this->db->query($sql);
		$retorno = $sql->fetchAll();

		if(isset($retorno[0]['texto'])){
			$retorno[0]['texto'] = utf8_encode($retorno[0]['texto']);
		}


		return $retorno;
	}

	public function verificaFale($html, $Id_Curso, $Id_Texto, $id_usuario, $nota){

		$Id_Usuario =  $_SESSION['id_usuario'];
		$_exercicio = 0;

		$html = str_replace("\"", "'",$html);
		$html = str_replace("falar()", "",$html);
		$html = str_replace("https://www.efidiomas.com.br/assets/imagens/microfone.png", "".BASE_URL."/assets/imagens/exercicio_concluido.png",$html);
		$html = utf8_decode($html);
		$Exercicios = "SELECT * FROM tb_Progresso WHERE Id_Aluno = $Id_Usuario AND Id_Curso = $Id_Curso";
		$Exercicios = $this->db->query($Exercicios);
		$dadosExercicios = $Exercicios->fetchAll();

		$_exercicio = $dadosExercicios[0]['Exercicio'];
		$_exercicio++;

		$_aula = $dadosExercicios[0]['Aula'];
		$_aula++;

		$UpdateExercicio = "UPDATE tb_Progresso SET Exercicio = ? WHERE Id_Aluno = ? AND Id_Curso = ?";
		$UpdateExercicio = $this->db->prepare($UpdateExercicio);
		$UpdateExercicio->bindParam(1, $_exercicio);
		$UpdateExercicio->bindParam(2, $Id_Usuario);
		$UpdateExercicio->bindParam(3, $Id_Curso);
		$UpdateExercicio->execute();

		if($_exercicio % 2 == 0){
				$UpdateExercicio = "UPDATE tb_Progresso SET Exercicio = ?, Aula = ? WHERE Id_Aluno = ? AND Id_Curso = ?";
				$UpdateExercicio = $this->db->prepare($UpdateExercicio);
				$UpdateExercicio->bindParam(1, $_exercicio);
				$UpdateExercicio->bindParam(2, $_aula);
				$UpdateExercicio->bindParam(3, $Id_Usuario);
				$UpdateExercicio->bindParam(4, $Id_Curso);
				$UpdateExercicio->execute();
		}else{
				$UpdateExercicio = "UPDATE tb_Progresso SET Exercicio = ? WHERE Id_Aluno = ? AND Id_Curso = ?";
				$UpdateExercicio = $this->db->prepare($UpdateExercicio);
				$UpdateExercicio->bindParam(1, $_exercicio);
				$UpdateExercicio->bindParam(2, $Id_Usuario);
				$UpdateExercicio->bindParam(3, $Id_Curso);
				$UpdateExercicio->execute();
		}

		$faleResolvido = "INSERT INTO tb_faleresolvido(texto,id_usuario,id_texto,nota) VALUES (?, ?, ?, ?)";
		$faleResolvido = $this->db->prepare($faleResolvido);
		$faleResolvido->bindParam(1, $html);
		$faleResolvido->bindParam(2, $id_usuario);
		$faleResolvido->bindParam(3, $Id_Texto);
		$faleResolvido->bindParam(4, $nota);
		$faleResolvido->execute();
	}
}
