<?php
class VerificarContratoAluno extends model{

	public function Verificar($curso,$usuario){

		$array = array();
		$sql = "SELECT * FROM tb_ContratoAluno inner join tb_Usuarios on (tb_ContratoAluno.Id_Usuario = tb_Usuarios.Id_Usuario) inner join tb_Cursos on (tb_ContratoAluno.Id_Curso = tb_Cursos.Id_Curso) where tb_ContratoAluno.Id_Usuario = $usuario and tb_ContratoAluno.Id_Curso = $curso";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}


	public function Aceitar($Curso,$Usuario){
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Y-m-d H:i:s');
        $Aceite = 1;
        $sql = "UPDATE tb_ContratoAluno SET Aceite = :Aceite,
            Data_Aceite = :Data_Aceite
            WHERE Id_Usuario = :Id_Usuario
            and   Id_Curso = :Id_Curso";
        $stmt = $this->db->prepare($sql);;
        $stmt->bindParam(':Aceite',$Aceite , PDO::PARAM_INT);
        $stmt->bindParam(':Data_Aceite', $date, PDO::PARAM_STR);
        $stmt->bindParam(':Id_Usuario', $Usuario, PDO::PARAM_INT);
        $stmt->bindParam(':Id_Curso', $Curso, PDO::PARAM_INT);
        $stmt->execute();
		 header('Location:' . BASE_URL . '/ameai/aula/' . $Curso . '/0/1');
         exit();
	}

	public function CriarContrato($Usuario,$Curso,$Aceite){

      if($Curso == 86){

      $sql = "SELECT * FROM tb_PacotePromocaoCurso WHERE Id_PacotePromocaoCurso = $Curso";
		$sql = $this->db->query($sql);
		$array = $sql->fetchAll();
		foreach ($array as $ids) {
			$id_curso = $ids['Ids_Cursos'];
		}

		$id_curso = explode(",", $ids[3]);

         foreach ($id_curso as $curso) {
        $sql = "INSERT INTO tb_ContratoAluno(Id_Usuario,Id_Curso,Aceite) VALUES (:Id_Usuario, :Id_Curso, :Aceite)";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":Id_Usuario",$Usuario);
		$sql->bindValue(":Id_Curso",$curso);
		$sql->bindValue(":Aceite",$Aceite);
		$sql->execute();
         }

     }else{

    $sql = "INSERT INTO tb_ContratoAluno(Id_Usuario,Id_Curso,Aceite) VALUES (:Id_Usuario, :Id_Curso, :Aceite)";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":Id_Usuario",$Usuario);
		$sql->bindValue(":Id_Curso",$Curso);
		$sql->bindValue(":Aceite",$Aceite);
		$sql->execute();
	}
	}
}
