<?php

namespace App\ModelEfi;

use Illuminate\Database\Eloquent\Model;
use App\Progresso;
use App\Capitulos;
use App\Cursos;

class InfosAmeai extends Model {

	public function getInfoCursoBkp($curso){

		$array = array();

		$sql = "SELECT * FROM tb_Cursos where Id_Curso = $curso and Excluido<> 1";
		$sql = $this->db->query($sql);
		if($sql->rowCount() > 0) {
			$array = $sql->fetchAll();
		}
		return $array;
	}

	public function getInfoCurso($curso){

		$sql = Cursos::find($curso);

		return $sql;
	}

	public function getInfoAulaBkp($curso,$ordem,$aba){

		$alunoFeitas = array();
		$dadosAula = array();
		$desabilitado_avancar = '';
		$desabilitado_voltar = '';
		$alertTutorial;

		if(isset($_SESSION['tipo_user'])){
			$usuario = $_SESSION['id_user'];
		}else{
			$usuario = $_SESSION['id_usuario'];
		}

		$aulasfeitas = "SELECT Aula,AlertBoasVindas,Exercicio FROM tb_Progresso INNER JOIN tb_Matriculas ON (tb_Progresso.Id_Aluno = tb_Matriculas.Id_Usuario) where tb_Progresso.Id_Aluno = $usuario and tb_Progresso.Id_Curso = $curso";
		$aulasfeitas = $this->db->query($aulasfeitas);
		$alunoFeitas =  $aulasfeitas->fetchAll();

		// if (!empty($alunoFeitas)) {
				$alertTutorial = $alunoFeitas['0']['AlertBoasVindas'];
		// }else {
		// 	$alertTutorial = 1;
		// 	$alunoFeitas['0']['Aula'] = 89;
		// 	$alunoFeitas['0']['Exercicio'] = 101;
		// }

		$quantidadeAulas = "SELECT * FROM tb_Capitulos where Id_Curso = $curso and Excluido <> 1";
		$quantidadeAulas = $this->db->query($quantidadeAulas);
		if($quantidadeAulas->rowCount() > 0) {
			$QuantAulas = $quantidadeAulas->rowCount();
			$Porcentagem1 = $alunoFeitas['0']['Aula']  * 100;
			$PorcentagemModulo = $Porcentagem1 / $QuantAulas ;
			$PorcentagemModuloArredondado = round ($PorcentagemModulo, 1, PHP_ROUND_HALF_DOWN);
			$QuantExercicio = $QuantAulas * 2;
			//verifica se já fez as 60 aulas
			if($alunoFeitas['0']['Exercicio'] == $QuantExercicio){
				$dadosAula[12] = 'positivo';
			}else{
				$dadosAula[12] = 'negativo';
			}

			if($ordem == 0){
				$desabilitado_voltar = 'disabled';
				$color_botao_pagina_voltar = '#C4C4C4';
				$dadosAula[10] = 'style="pointer-events: none; cursor: default;"';
			}else{
				$color_botao_pagina_voltar = '#004764';
				$dadosAula[10] = "";
			}

			if($ordem == $QuantAulas){
				//desabilita o botão avançar, pois ja foi feito as 60 aulas
				$desabilitado_avancar = 'disabled';
				$color_botao_pagina_seguir = '#C4C4C4';
				$dadosAula[9] = 'style="pointer-events: none; cursor: default;"';
			}else if($ordem == $alunoFeitas['0']['Aula']){
				$desabilitado_avancar = 'disabled';
				$color_botao_pagina_seguir = '#C4C4C4';
				$dadosAula[9] = 'style="pointer-events: none; cursor: default;"';
			}else{
				$color_botao_pagina_seguir = '#004764';
				$dadosAula[9] = "";
			}
		}
		$dadosAula[0] = $PorcentagemModuloArredondado;
		$dadosAula[1] = $color_botao_pagina_voltar;
		$dadosAula[2] = $color_botao_pagina_seguir;
		$dadosAula[3] = $desabilitado_voltar;
		$dadosAula[4] = $desabilitado_avancar;
		$dadosAula[5] = $curso;
		$dadosAula[6] = $ordem + 1;
		$dadosAula[7] = $ordem - 1;
		$dadosAula[8] = $ordem;
		$dadosAula[11] = $alertTutorial;
		return $dadosAula;
	}

	public function getInfoAula($curso,$ordem,$aba,$usuario){

		$alunoFeitas = Progresso::join('tb_Matriculas', 'tb_Progresso.Id_Aluno', 'tb_Matriculas.Id_Usuario')
		->where('tb_Progresso.Id_Aluno', $usuario)
		->where('tb_Progresso.Id_Curso', $curso)
		->select('Aula', 'Exercicio')
		->get();

		$alertTutorial = 0;

		$quantidadeAulas = Capitulos::where('Id_Curso', $curso)
		->where('Excluido', '<>', 1)
		->get();

		if($quantidadeAulas->count() > 0) {
			$QuantAulas = $quantidadeAulas->count();
			$Porcentagem1 = $alunoFeitas['0']['Aula']  * 100;
			$PorcentagemModulo = $Porcentagem1 / $QuantAulas ;
			$PorcentagemModuloArredondado = round ($PorcentagemModulo, 1, PHP_ROUND_HALF_DOWN);
			$QuantExercicio = $QuantAulas * 2;
			//verifica se já fez as 60 aulas
			if($alunoFeitas['0']['Exercicio'] == $QuantExercicio){
				$dadosAula[12] = 'positivo';
			}
			else{
				$dadosAula[12] = 'negativo';
			}

			if($ordem == 0){
				$desabilitado_voltar = 'disabled';
				$color_botao_pagina_voltar = '#C4C4C4';
				$dadosAula[10] = 'style="pointer-events: none; cursor: default;"';
			}else{
				$color_botao_pagina_voltar = '#004764';
				$dadosAula[10] = "";
				$desabilitado_voltar = '';
			}

			if($ordem == $QuantAulas){
				//desabilita o botão avançar, pois ja foi feito as 60 aulas
				$desabilitado_avancar = 'disabled';
				$color_botao_pagina_seguir = '#C4C4C4';
				$dadosAula[9] = 'style="pointer-events: none; cursor: default;"';
			}else if($ordem == $alunoFeitas['0']['Aula']){
				$desabilitado_avancar = 'disabled';
				$color_botao_pagina_seguir = '#C4C4C4';
				$dadosAula[9] = 'style="pointer-events: none; cursor: default;"';
			}else{
				$color_botao_pagina_seguir = '#004764';
				$dadosAula[9] = "";
				$desabilitado_avancar = '';
			}
		}
		$dadosAula[0] = $PorcentagemModuloArredondado;
		$dadosAula[1] = $color_botao_pagina_voltar;
		$dadosAula[2] = $color_botao_pagina_seguir;
		$dadosAula[3] = $desabilitado_voltar;
		$dadosAula[4] = $desabilitado_avancar;
		$dadosAula[5] = $curso;
		$dadosAula[6] = $ordem + 1;
		$dadosAula[7] = $ordem - 1;
		$dadosAula[8] = $ordem;
		$dadosAula[11] = $alertTutorial;
		return $dadosAula;
	}
}
