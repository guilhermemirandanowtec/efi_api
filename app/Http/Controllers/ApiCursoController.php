<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Capitulos;
use App\Texto;
use App\Questions;
use App\Matriculas;
use App\TestStatus;
use App\TestDef;
use App\Licao;
use App\Usuarios;
use App\Progresso;
use App\Ameai;
use App\Cursos;
use App\ModelEfi\conteudoAula;
use App\ModelEfi\InfosAmeai;
use App\ModelEfi\inserilogAmeai;
use App\LogAcessoAMEAI;
use Carbon\Carbon;
use App\EscrevaResolvido;
use App\FaleResolvido;
use App\RespostasRecursos;
use App\ExerciciosExtras;

class ApiCursoController extends Controller
{
    public function getDetalhesCurso(Request $request){
      $capitulos = Capitulos::where('Id_Curso', $request->id_curso)
      ->where('Excluido',0)
      ->orderBy('Ordem')
      ->select('Nome_Capitulo', 'Id_Capitulo')
      ->get();

      $sub_capitulos = Licao::join('tb_textos','tb_textos.Id_Licao','tb_Licao.Id_Licao')
      ->where('tb_Licao.Id_Curso',$request->id_curso)
      ->where('tb_textos.Excluido','<>',1)
      ->orderBy('tb_textos.Ordem')
      ->select('tb_Licao.Id_Licao as id_licao', 'tb_Licao.Id_Curso as id_curso', 'tb_textos.Id_Capitulo as id_capitulo', 'tb_Licao.Nome_Licao as nome_licao', 'tb_textos.Ordem as ordem')
      ->get();

      // $test_def = TestDef::where('Id_Curso',$request->id_curso)->first();


      // $status = TestStatus::where('id_test',$test_def->id_TestDef)
      // ->where('id_Usuario', $request->id_usuario)
      // ->orderBy('id_testStatus','desc')
      // ->select('pts','done')
      // ->first();

      return response()->json(['capitulos' => $capitulos, 'sub_capitulos' => $sub_capitulos]);
    }

    public function getTexto(Request $request){
      $textos = Texto::where('tb_textos.Id_Curso', $request->id_curso)
      ->where('tb_textos.Excluido',0)
      ->where('tb_Licao.Tipo_Licao','<>','ouca')
      ->join('tb_Licao','tb_Licao.Id_Licao','tb_textos.Id_Licao')
      ->join('tb_Capitulos','tb_Capitulos.Id_Capitulo','tb_Licao.Id_Capitulo')
      ->orderBy('tb_Capitulos.Ordem')
      ->orderBy('tb_Licao.Ordem')
      ->get();

      $pagina_atual = Matriculas::where('Id_Curso',$request->id_curso)
      ->where('Id_Usuario',$request->id_usuario)
      ->where('Excluido',0)
      ->first();

      return response()->json(['textos' => $textos, 'pagina_atual' => $pagina_atual]);
    }

    public function getProva(Request $request){
      $questoes = Questions::where('Id_Curso',$request->id_curso)
      ->where('Excluido',0)
      ->get();

      $test_def = TestDef::where('Id_Curso',$request->id_curso)->first();

      TestStatus::create([
        'id_Usuario' => $request->id_usuario,
        'id_test' => $test_def->id_TestDef,
        'done' => 1,
        'pts' => 0,
        'startDate' => Carbon::now()->toDatetimeString(),
        'finishDate' => 0
      ]);

      return response()->json(['questoes' => $questoes, 'test_def' => $test_def]);
    }

    public function saveProva(Request $request){

      TestStatus::where('id_test',$request->id_test)
      ->where('Id_Usuario', $request->id_usuario)
      ->update([
        'Questoes_Sorteadas' => $request->questoes,
        'userSolution' => $request->solution,
        'pts' => $request->pts,
        'finishDate' => Carbon::now()->toDatetimeString(),
        'done' => 1,
      ]);

      return response()->json(['success' => true]);
    }

    public function updatePagina(Request $request){
      $matricula = Matriculas::where('Id_Curso',$request->id_curso)
      ->where('Id_Usuario',$request->id_usuario)
      ->where('Excluido',0)
      ->first();

      if($matricula->UltimaPagina < $request->pagina){
        $matricula->UltimaPagina = $request->pagina;
        $matricula->save();
      }
    }

    public function getAulaBkp($curso, $ordem, $aba){

      if ($aba > 8) {
        return redirect('/api/get_Aula/'. $curso . '/' . $ordem . '/1');
      }
      if ($curso == null or $ordem == null or $aba == null) {
        abort(405);
      }

      $usuario = 5;

      $usuarios = new Usuarios();
      $dadosUser = $usuarios->getDados(5);
      $conteudoaula = new conteudoAula();
      $Aula = $conteudoaula->getConteudo($curso, $ordem, $aba, $usuario);
      $InfoRecurso = $conteudoaula->InfoRecursos($curso, $ordem, $usuario);
      $ConteudoSumario = $conteudoaula->getSumario($curso);
      $infosameai = new InfosAmeai();
      $infoAula = $infosameai->getInfoAula($curso, $ordem, $aba, $usuario);
      $infosCurso = $infosameai->getInfoCurso($curso, $ordem);
      $logameai = new inserilogAmeai();
      $logameai->inseriLog($curso, $ordem, $usuario);

      return view('aula', compact('dadosUser', 'Aula', 'InfoRecurso', 'ConteudoSumario', 'infoAula', 'infosCurso'));
    }

    public function getAula($id_curso, $id_usuario, $id_capitulo, $tab){

      $progresso = Progresso::where('Id_Aluno', $id_usuario)
      ->where('Id_Curso',$id_curso)
      ->select('Aula','Exercicio')
      ->first();

      if($id_capitulo == 0){
        $aula = $progresso->Aula;
      }
      else{
        $aula = $id_capitulo;
      }

      $curso = Ameai::getInfoCurso($id_curso);
      $sumario = Ameai::getSumario($id_curso);
      $class = Ameai::getClassInfo($id_curso, $aula);
      $texto = Ameai::getClassContent($id_curso, $aula, $tab, $id_usuario);
      $materialApoio = Ameai::getMateriaisApoioCurso($id_curso);

      $aluno = Usuarios::find($id_usuario);
      $tab = $tab;

      return view('aula2', compact('progresso', 'curso', 'sumario', 'class', 'texto', 'materialApoio', 'aluno', 'tab', 'aula'));
    }

    public function salvarAcessoAmeai(Request $request) {

      if($request->lastLog == 0){
        $log_acesso_ameai = LogAcessoAMEAI::create([
          'id_usuario' => $request->id_usuario,
          'id_curso' => $request->id_curso,
          'id_capitulo' => $request->id_capitulo,
          'tab' => $request->tab,
          'hora_inicio' => Carbon::now()->format('Y-m-d H:i:s'),
          'hora_final' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

      }
      else{
        $log_acesso_ameai = LogAcessoAMEAI::find($request->lastLog);
        $log_acesso_ameai->id_usuario = $request->id_usuario;
        $log_acesso_ameai->id_curso = $request->id_curso;
        $log_acesso_ameai->id_capitulo = $request->id_capitulo;
        $log_acesso_ameai->tab = $request->tab;
        $log_acesso_ameai->hora_final = Carbon::now()->format('Y-m-d H:i:s');

        $log_acesso_ameai->save();
      }

      if($request->lastLog != 0){
        return response()->json(['lastLog' => $request->lastLog]);
      }
      else{
        return response()->json(['lastLog' => $log_acesso_ameai->id_logAcessoAMEAI]);
      }

    }

    public function getTabClassText(Request $request){
      $abas = [
          "1" => "leia",
          "2" => "fale",
          "3" => "ouca", // Não existe
          "4" => "gramatica",
          "5" => "escreva",
          "6" => "vocabulario",
          "7" => "video",
          "8" => "saiba mais",
          "10" => "exercicios"
      ];

      if($request->tab == 10){


        $curso = Cursos::find($request->id_curso);

        $exercicio = Ameai::buscarExercicios($request->id_curso, $request->capitulo);


        $app = app();
        $texto = $app->make('stdClass');

        $texto->texto = $exercicio['html'];
        $texto->nome_capitulo = $exercicio['capitulo']->Nome_Capitulo;
        $texto->nome_curso = $curso->Nome_Curso;
        $texto->id_texto = null;

        $texto = json_encode($texto);

      }
      else{
        $texto = Texto::select('tb_textos.texto as texto', 'Nome_Capitulo', 'Nome_Curso', 'Id_texto', 'tb_Capitulos.Ordem as Ordem')
        ->join('tb_Capitulos', 'tb_textos.Id_Capitulo', 'tb_Capitulos.Id_Capitulo')
        ->join('tb_Licao', 'tb_textos.Id_Licao', 'tb_Licao.Id_Licao')
        ->join('tb_Cursos', 'tb_textos.Id_Curso', 'tb_Cursos.Id_Curso')
        ->where('Tipo_Licao', $abas[$request->tab])
        ->where('tb_textos.Id_Curso', $request->id_curso)
        ->where('tb_Capitulos.Ordem', $request->capitulo)
        ->where('tb_textos.Excluido', '<>', 1)
        ->first();

        if($abas[$request->tab] == 'escreva'){

          $completedWrite = Ameai::completedWrite($request->id_aluno, $texto['Id_texto']);

          if($completedWrite) {
            $texto->texto = $completedWrite->texto;
          }

        }
        else if($abas[$request->tab] == 'fale') {


          $completedTalk = Ameai::completedTalk($request->id_aluno, $texto['Id_texto']);

          if ($completedTalk) {
            $texto->texto = $completedTalk->texto;
          }

        }
      }

      return $texto;

    }

    public function completedWrite(Request $request) {

      $sql = EscrevaResolvido::select('texto','nota')
      ->where('id_usuario',$request->id_usuario)
      ->where('id_texto', $request->id_texto)
      ->first();

      return response()->json(['retorno' => $sql]);

    }

    public function completedTalk(Request $request) {

      $sql = FaleResolvido::select('texto')
      ->where('id_usuario',$request->id_usuario)
      ->where('id_texto', $request->id_texto)
      ->first();

      return response()->json(['retorno' => $sql]);

    }

    public function getWriteAnswear(Request $request){

      $sql = RespostasRecursos::where('Id_texto', $request->id_texto)
      ->where('Excluido',0)
      ->first();

      return $sql;
    }

    public function finalizarEscreva(Request $request){

      EscrevaResolvido::create([
        'id_usuario' => $request->id_usuario,
        'id_texto' => $request->id_texto,
        'nota' => $request->nota,
        'texto' => $request->texto
      ]);

      return $this->addProgresso($request->id_usuario, $request->id_texto);

    }

    public function finalizarFale(Request $request) {

      FaleResolvido::create([
        'id_usuario' => $request->id_usuario,
        'id_texto' => $request->id_texto,
        'nota' => $request->nota,
        'texto' => $request->texto
      ]);

      return $this->addProgresso($request->id_usuario, $request->id_texto);

    }

    public function addProgresso($id_aluno, $id_texto) {

      $progresso = Progresso::select('tb_Progresso.Id as Id', 'tb_Progresso.Aula as Aula', 'tb_Progresso.Exercicio as Exercicio', 'tb_textos.Id_Curso as Id_Curso')
      ->join('tb_textos', 'tb_textos.Id_Curso', 'tb_Progresso.Id_Curso')
      ->where('tb_Progresso.Id_Aluno', $id_aluno)
      ->where('tb_textos.Id_texto', $id_texto)
      ->first();

      $id_curso = $progresso->Id_Curso;
      $progressoExercicio = $progresso->Exercicio;
      $progressoExercicio ++;

      $progresso = Progresso::find($progresso->Id);

      if($progressoExercicio % 2 == 0){
        $aula = $progressoExercicio / 2;
        $progresso->Aula = $aula;
      }

      $progresso->Exercicio = $progressoExercicio;

      $progresso->save();

      return $progressoExercicio;

    }

    public function getSumario(Request $request){

      $progresso = Progresso::where('Id_Aluno', $request->id_usuario)
      ->where('Id_Curso',$request->id_curso)
      ->select('Aula','Exercicio')
      ->first();

      $sumario = Cursos::select('tb_Cursos.Nome_Curso as nome_curso','tb_Capitulos.Ordem as ordem','tb_Capitulos.Nome_Capitulo as nome_capitulo')
      ->join('tb_Capitulos', 'tb_Cursos.Id_Curso', 'tb_Capitulos.Id_Curso')
      ->where('tb_Cursos.Id_Curso', $request->id_curso)
      ->where('tb_Capitulos.Excluido',0)
      ->orderBy('tb_Capitulos.Ordem')
      ->get();

      return response()->json(['sumario' => $sumario, 'progresso' => $progresso]);
    }

    public function cursoAtual(Request $request){
      $texto = Ameai::getClassContent($request->id_curso, $request->capitulo, $request->ordem, $request->id_aluno);

      return response()->json(['texto' => $texto]);
    }

    public function salvaTentativa(Request $request){

      $array_respostas = json_decode($request->respostas, true);

      foreach ($array_respostas as $resposta) {
        ExerciciosExtras::salva_tentativa($resposta['id'], $resposta['resposta'], $request->id_aluno);
      }

      $exercicios = ExerciciosExtras::select('id', 'questao', 'alternativa_a', 'alternativa_b', 'alternativa_c', 'alternativa_d', 'alternativa_e')
      ->where('id_capitulo', $request->id_capitulo)
      ->whereNull('deleted_at')
      ->get();

      $html = '';
      foreach ($exercicios as $index => $exercicio) {
         $status =  $array_respostas[$index]['resposta'] == $exercicio['alternativa_a'] ? "<span style='padding-left: 15px;color: green;'><i class='fas fa-check'></i></span>" : "<span style='padding-left: 15px;color: red;'><i class='fas fa-times'></i></span>";
         $html .= '<div style="margin: 10px;font-weight: bold;font-size: large;">'. ((int) $index + 1) . ' - ' . $exercicio['questao'] . '</div>';
          $array = [$exercicio['alternativa_a'], $exercicio['alternativa_b'], $exercicio['alternativa_c'], $exercicio['alternativa_d'], $exercicio['alternativa_e']];
          shuffle($array);
          foreach ($array as $key => $item) {

             $style = $item == $exercicio['alternativa_a'] ? "color: green;font-weight: bold;" : "";
             $aux = $array_respostas[$index]['resposta'] == $item  ? $status : "";
              $html .= '<label class="custom-control custom-radio custom-control-inline">
                      <input id="' . $exercicio['id'] . '"
                      type="radio" value="' . $item . '"
                      name="questao' . ((int) $index + 1) . '"
                      class="custom-control-input"><span style="'.$style.'" class="custom-control-label">'. $item . $aux . '</span>
                      </label><br>';
          }
        }
        $html .= '<button onclick="refazer('.$request->id_capitulo.')" class="btn btn-primary">Refazer</button>';
        return $html;
    }

    public function buscarExercicios(Request $request){

      $exercicios = ExerciciosExtras::select('id', 'questao', 'alternativa_a', 'alternativa_b', 'alternativa_c', 'alternativa_d', 'alternativa_e')
      ->where('id_capitulo', $request->Id_Capitulo)
      ->whereNull('deleted_at')
      ->get();

      $html = '';

      if(sizeof($exercicios) <= 0){
          $html .='Ainda não existe exercicios para está aula.';
          return compact('html','capitulo');
      }

      foreach ($exercicios as $index => $exercicio) {

        $html .='<div style="margin: 10px;font-weight: bold;font-size: large;">'. ((int) $index + 1).' - '. $exercicio['questao'] . '</div>';
        $array = [$exercicio['alternativa_a'], $exercicio['alternativa_b'], $exercicio['alternativa_c'], $exercicio['alternativa_d'],$exercicio['alternativa_e']];
        shuffle($array);
        foreach ($array as $key => $item) {
            $html.= '<label class="custom-control custom-radio custom-control-inline">
                     <input id="'. $exercicio['id'].'" type="radio" value="'. $item .'" name="questao' . ((int) $index + 1) . '" class="custom-control-input"><span class="custom-control-label">' . $item . '</span>
                     </label><br>';
        }
      }
      $html .= '<button onclick="get_respostas('.$request->Id_Capitulo.')" class="btn btn-primary">Verificar</button>';
      return compact('html','capitulo');
    }
}
