<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;
use App\Matriculas;
use App\Progresso;

class ApiUsersController extends Controller
{
    public function login(Request $request){
      $usuario = Usuarios::where('Email',$request->email)->where('Excluido',0)->first();

      if($usuario){

        $senha = $this->encryptPassword($request->senha);

        if($usuario->Senha == $senha){
          return response()->json(['success'=>$usuario]);
        }
        else{
          return response()->json(['error'=>'Senha incorreta.']);
        }
      }
      else{
        return response()->json(['error'=>'Usuário não existe.']);
      }
    }

    public function getCursos(Request $request){

      $cursos = Matriculas::select('Nome_Curso as nome_curso', 'ImagemCurso as img', 'tb_Cursos.Id_Curso as curso_id', 'Aula as aula')
      ->join('tb_Cursos','tb_Cursos.Id_Curso','tb_Matriculas.Id_Curso')
      ->join('tb_AC','tb_AC.Id_Curso','tb_Cursos.Id_Curso')
      ->join('tb_Progresso','tb_Progresso.Id_Curso','tb_Cursos.Id_Curso')
      ->where('tb_Matriculas.Id_Usuario',$request->user_id)
      ->where('tb_Matriculas.Excluido',0)
      ->where('tb_Progresso.Id_Aluno',$request->user_id)
      ->orderBy('Nome_Curso')
      ->get();

      return response()->json(['cursos' => $cursos]);
    }

    public function getResultadoCursos(Request $request){

      $cursos = Matriculas::select('Nome_Curso','Aula','Exercicio', 'ImagemCurso')
      ->join('tb_Cursos', 'tb_Cursos.Id_curso', 'tb_Matriculas.Id_Curso')
      ->join('tb_AC', 'tb_AC.Id_Curso', 'tb_Matriculas.Id_Curso')
      ->join('tb_Progresso', 'tb_Progresso.Id_Curso', 'tb_Matriculas.Id_Curso')
      ->where('tb_Progresso.Id_Aluno', $request->user_id)
      ->where('tb_Matriculas.Id_Usuario', $request->user_id)
      ->where('tb_Matriculas.Excluido', 0)
      ->where('tb_Cursos.Excluido', 0)
      ->where('tb_AC.Excluido', 0)
      ->get();

      return response()->json(['cursos' => $cursos]);
    }

    public function salvarCadastro(Request $request){
      $user = Usuarios::find($request->user_id);

      $user->Nome = $request->nome;
      $user->TelCelular = $request->celular;
      $user->Endereco = $request->endereco;
      $user->Numero = $request->numero;
      $user->Bairro = $request->bairro;
      $user->Cidade = $request->cidade;
      $user->Estado = $request->estado;
      $user->Complemento = $request->complemento;
      $user->CEP = $request->cep;

      if($request->senha != ''){
        $user->Senha = $request->senha;
      }

      $user->save();

      return response()->json(['user' => $user]);
    }

    public function encryptPassword($senha){
      $salt = "$&OK";
      return md5($senha . $salt);
    }
}
