<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matriculas extends Model
{
    protected $table = 'tb_Matriculas';
    protected $primaryKey = 'Id_Matricula';

    protected $fillable = ['UltimaPagina','PorcentagemNota'];
}
