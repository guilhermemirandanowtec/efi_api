<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cursos;
use App\Capitulos;
use App\Texto;
use App\EscrevaResolvido;
use App\FaleResolvido;
use App\MaterialApoio;

class Ameai extends Model
{
  public static function getInfoCurso($curso){

    $sql = Cursos::find($curso);

    return $sql;
  }

  public static function getSumario($id_curso) {

    $sumario = Cursos::select('tb_Cursos.Nome_Curso as nome_curso','tb_Capitulos.Ordem as ordem','tb_Capitulos.Nome_Capitulo as nome_capitulo')
    ->join('tb_Capitulos', 'tb_Cursos.Id_Curso', 'tb_Capitulos.Id_Curso')
    ->where('tb_Cursos.Id_Curso', $id_curso)
    ->where('tb_Capitulos.Excluido',0)
    ->orderBy('tb_Capitulos.Ordem')
    ->get();

    return $sumario;
  }

  public static function getClassInfo($id_curso, $capitulo) {

    $class = Capitulos::select('Nome_Capitulo as nome_capitulo', 'tb_Capitulos.Id_Capitulo as id_capitulo', 'tb_Capitulos.Ordem as ordem')
    ->join('tb_textos', 'tb_textos.Id_Capitulo', 'tb_Capitulos.Id_Capitulo')
    ->where('tb_textos.Id_Curso', $id_curso)
    ->where('tb_Capitulos.Ordem', $capitulo)
    ->where('tb_Capitulos.Excluido', 0)
    ->where('tb_textos.Excluido', 0)
    ->first();

    return $class;
  }

  public static function getClassContent($id_curso, $capitulo, $ordem, $id_aluno) {

    $abas = [
        "1" => "leia",
        "2" => "fale",
        "3" => "ouca", // Não existe
        "4" => "gramatica",
        "5" => "escreva",
        "6" => "vocabulario",
        "7" => "video",
        "8" => "saiba mais",
        "10" => "exercicios"
    ];

    if($ordem == 10){


      $curso = Cursos::find($id_curso);

      $exercicio = Ameai::buscarExercicios($id_curso, $capitulo);

      $app = app();
      $texto = $app->make('stdClass');

      $texto->texto = $exercicio['html'];
      $texto->nome_capitulo = $exercicio['capitulo']->Nome_Capitulo;
      $texto->nome_curso = $curso->Nome_Curso;
      $texto->id_texto = null;

    }
    else{
      $texto = Texto::select('tb_textos.texto as texto', 'Nome_Capitulo as nome_capitulo', 'Nome_Curso as nome_curso', 'Id_texto as id_texto')
      ->join('tb_Capitulos', 'tb_textos.Id_Capitulo', 'tb_Capitulos.Id_Capitulo')
      ->join('tb_Licao', 'tb_textos.Id_Licao', 'tb_Licao.Id_Licao')
      ->join('tb_Cursos', 'tb_textos.Id_Curso', 'tb_Cursos.Id_Curso')
      ->where('Tipo_Licao', $abas[$ordem])
      ->where('tb_textos.Id_Curso', $id_curso)
      ->where('tb_Capitulos.Ordem', $capitulo)
      ->where('tb_textos.Excluido', '<>', 1)
      ->first();

      if($ordem == 5){

        $completedWrite = Ameai::completedWrite($id_aluno, $texto->id_texto);

        if($completedWrite) {
          $texto->texto = $completedWrite->texto;
        }

      }
      else if($ordem == 2) {

        $completedTalk = Ameai::completedTalk($id_aluno, $texto->id_texto);

        if ($completedTalk) {
          $texto->texto = $completedTalk->texto;
        }

      }
    }
    return $texto;


  }

  public static function buscarExercicios($id_curso, $ordem){

    $capitulo = Capitulos::where('Id_curso',$id_curso)
    ->where('Ordem',$ordem)
    ->where('Excluido',0)
    ->first();

    $exercicios = ExerciciosExtras::select('id', 'questao', 'alternativa_a', 'alternativa_b', 'alternativa_c', 'alternativa_d', 'alternativa_e')
    ->where('id_capitulo', $capitulo->Id_Capitulo)
    ->whereNull('deleted_at')
    ->get();

    $html = '';

    if(sizeof($exercicios) <= 0){
        $html .='Ainda não existe exercicios para está aula.';
        return compact('html','capitulo');
    }

    foreach ($exercicios as $index => $exercicio) {

      $html .='<div style="margin: 10px;font-weight: bold;font-size: large;">'. ((int) $index + 1).' - '. $exercicio['questao'] . '</div>';
      $array = [$exercicio['alternativa_a'], $exercicio['alternativa_b'], $exercicio['alternativa_c'], $exercicio['alternativa_d'],$exercicio['alternativa_e']];
      shuffle($array);
      foreach ($array as $key => $item) {
          $html.= '<label class="custom-control custom-radio custom-control-inline">
                   <input id="'. $exercicio['id'].'" type="radio" value="'. $item .'" name="questao' . ((int) $index + 1) . '" class="custom-control-input"><span class="custom-control-label">' . $item . '</span>
                   </label><br>';
      }
    }
    $html .= '<button onclick="get_respostas('.$capitulo->Id_Capitulo.')" class="btn btn-primary">Verificar</button>';
    return compact('html','capitulo');
  }

  public static function completedWrite($id_usuario,$id_texto) {

    $escreva = EscrevaResolvido::select('texto','nota')
    ->where('id_usuario', $id_usuario)
    ->where('id_texto', $id_texto)
    ->first();

    return $escreva;

  }

  public static function completedTalk($id_usuario,$id_texto) {

    $fale = FaleResolvido::select('texto','nota')
    ->where('id_usuario', $id_usuario)
    ->where('id_texto', $id_texto)
    ->first();

    return $fale;

  }

  public static function getMateriaisApoioCurso($id_curso) {

    $sql = MaterialApoio::join('tb_Capitulos', 'tb_Capitulos.Id_Capitulo', 'tb_material_apoio.id_capitulo')
    ->where('Id_Curso', $id_curso)
    ->whereNull('tb_material_apoio.deleted_at')
    ->whereNull('tb_Capitulos.deleted_at')
    ->orderBy('tb_Capitulos.Ordem')
    ->get();

    return $sql;
  }
}
