<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'ApiUsersController@login');
Route::post('get_Cursos', 'ApiUsersController@getCursos');
Route::post('get_Resultados_Cursos', 'ApiUsersController@getResultadoCursos');
Route::post('get_Detalhes_Curso', 'ApiCursoController@getDetalhesCurso');
Route::post('get_Texto', 'ApiCursoController@getTexto');
Route::post('get_Prova', 'ApiCursoController@getProva');
Route::post('update_pagina', 'ApiCursoController@updatePagina');
Route::post('save_prova', 'ApiCursoController@saveProva');
Route::post('salvar_cadastro', 'ApiUsersController@salvarCadastro');

// Route::get('ameai_mobile/aula/{curso}/{ordem}/{aba}', 'ApiCursoController@getAula');
Route::get('aula/{id_curso}/{id_usuario}/{capitulo}/{tab}', 'ApiCursoController@getAula');
Route::post('curso_atual', 'ApiCursoController@cursoAtual');

Route::post('salvarAcessoAmeai', 'ApiCursoController@salvarAcessoAmeai');

Route::post('getTabClassText', 'ApiCursoController@getTabClassText');

Route::post('getWriteAnswear', 'ApiCursoController@getWriteAnswear');

Route::post('finalizar_escreva', 'ApiCursoController@finalizarEscreva');
Route::post('finalizar_fale', 'ApiCursoController@finalizarFale');

Route::post('completedWrite','ApiCursoController@completedWrite');
Route::post('completedTalk','ApiCursoController@completedTalk');

Route::post('getSumario', 'ApiCursoController@getSumario');

Route::post('salva_tentativa', 'ApiCursoController@salvaTentativa');
Route::post('buscar_exercicios', 'ApiCursoController@buscarExercicios');
