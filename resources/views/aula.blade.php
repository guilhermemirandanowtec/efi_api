<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>EFI Mobile</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="/assets/js/jquery.min.js"></script>
  <link href="/assets/css/mobile/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/nivo-lightbox.css" rel="stylesheet" />
  <link href="/assets/css/default.css" rel="stylesheet" type="text/css" />
  <link href="/assets/css/animate.css" rel="stylesheet" />
  <link href="/assets/css/style.css" rel="stylesheet">
  <link href="/assets/css/color/default.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/5.0.4/sweetalert2.min.css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/sweetalert2/5.0.4/sweetalert2.js"></script>
  <!-- Latest compiled and minified JavaScript -->

  <script src="/assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="http://www.google.com.br/jsapi"></script>
  <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <script type="text/javascript" src="/assets/js/ameai.js"></script>
  <link rel="stylesheet" type="text/css" href="/assets/css/jquery.fancybox.css" />

  <script type="text/javascript" src="/assets/js/jquery.fancybox.js"></script>
  <style>

  @media (max-width: 640px)
  {
    img {
      max-width:250px;
      max-height:200px;
      width: auto;
      height: auto;
    }

    #video{
      max-width:400px;
      max-height:350px;
      width: auto;
      height: auto;
    }
  }

  input{
    max-width: 100% !important;
  }


</style>

</head>

<body>
  <!-- Section: intro -->
  <br><br><br>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">


        <div class="container">
          <ul id="gn-menu" class="gn-menu-main">
            <li class="gn-trigger">
              <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
              <nav class="gn-menu-wrapper">
                <div class="gn-scroller">
                  <ul class="gn-menu">
                    <li style="background-color:#00455e; color:white; font-size:14px;"><b>{{utf8_encode($infosCurso['0']['Nome_Curso']).' - '.utf8_encode($Aula[1])}}</b></li>
                    <li style="{{$Aula[3] == 1 ? 'background-color:#f7d611;' : ''}}">
                      <a href="/ameai_mobile/aula/{{$infoAula[5]}}/{{$infoAula[8]}}/1" onclick="AlertCarregandoRecurso('Ouça')" style=""><div class="material-icons">&#xe023;</div>&ensp;Listen</a>
                    </li>
                    <li style="{{$Aula[3] == 2 ? 'background-color:#f7d611;' : ''}}">
                      <a href="/ameai_mobile/aula/{{$infoAula[5]}}/{{$infoAula[8]}}/2" onclick="AlertCarregandoRecurso('Fale')"><div class="material-icons">&#xe31d;</div>&ensp;Talk</a>
                    </li>
                    <li style="{{$Aula[3] == 3 ? 'background-color:#f7d611;' : ''}}">
                      <a href="/ameai_mobile/aula/{{$infoAula[5]}}/{{$infoAula[8]}}/3" onclick="AlertCarregandoRecurso('Leia')"><div class="material-icons">&#xe8f4;</div>&ensp;Read</a>
                    </li>
                    <li style="{{$Aula[3] == 5 ? 'background-color:#f7d611;' : ''}}">
                      <a href="/ameai_mobile/aula/{{$infoAula[5]}}/{{$infoAula[8]}}/5" onclick="AlertCarregandoRecurso('Escreva')"><div class="material-icons">&#xe31a;</div>&ensp;Write</a>
                    </li>
                    <li style="{{$Aula[3] == 6 ? 'background-color:#f7d611;' : ''}}">
                      <a href="/ameai_mobile/aula/{{$infoAula[5]}}/{{$infoAula[8]}}/6" onclick="AlertCarregandoRecurso('Media')"><div class="material-icons">&#xe04a;</div>&ensp;Media</a>
                    </li>
                    <li style="{{$Aula[3] == 7 ? 'background-color:#f7d611;' : ''}}">
                      <a href="/ameai_mobile/aula/{{$infoAula[5]}}/{{$infoAula[8]}}/7" onclick="AlertCarregandoRecurso('Vocabulário')"><div class="material-icons">&#xe85d;</div>&ensp;Vocabulary</a>
                    </li>
                    <li style="{{$Aula[3] == 4 ? 'background-color:#f7d611;' : ''}}">
                      <a href="/ameai_mobile/aula/{{$infoAula[5]}}/{{$infoAula[8]}}/4" onclick="AlertCarregandoRecurso('Gramática')"><div class="material-icons">&#xe86d;</div>&ensp;Grammar</a>
                    </li>
                    <li style="{{$Aula[3] == 8 ? 'background-color:#f7d611;' : ''}}">
                      <a href="/ameai_mobile/aula/{{$infoAula[5]}}/{{$infoAula[8]}}/8" onclick="AlertCarregandoRecurso('Saiba Mais')"><div class="material-icons">&#xe887;</div>&ensp;Know More</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" id="navbardrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size:18px;">EFI</a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="/" style="font-size:18px;text-align:center;"><b>Espaço Vip</b></a>
                <hr></hr>
                <a class="dropdown-item" href="#" onclick="ManualAluno()" style="font-size:18px;text-align:center;"><b>Manual do Aluno</b></a>
                <hr></hr>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal" style="font-size:18px;text-align:center;"><b>Sumário</b></a>
                <hr></hr>
                <a class="dropdown-item" href="#" style="font-size:18px;text-align:center;"><b>Forum</b></a>
              </div>
            </li>
            <li>
              <a href="{{$infoAula[3] == 'disabled' ? '#' : '/ameai_mobile/aula/'.$infoAula[5].'/'.$infoAula[7].'/1'}}" style="font-size:24px;display:inline;" class="fa">&#xf053;</a>
            </li>
            <li>
              <a href="{{$infoAula[4] == 'disabled' ? '#' : '/ameai_mobile/aula/'.$infoAula[5].'/'.$infoAula[6].'/1'}}" style="font-size:24px;display:inline;" class="fa">&#xf054;</a>
            </li>
            <li class="hidden-xs">
              <ul class="company-social"></ul>
            </li>
          </ul>
        </div>

        <div id="conteudo">
          @php
            $conteudo = str_replace("-moz-column-count: 2;-webkit-column-count: 2;", "-moz-column-count: 1;-webkit-column-count: 1;", $Aula[0]);
            $conteudo = str_replace("-moz-column-count: 3;-webkit-column-count: 3;", "-moz-column-count: 1;-webkit-column-count: 1;", $conteudo);
            $conteudo = str_replace("onmouseup", "ontouchend",$conteudo);
            $conteudo = str_replace("onmousedown", "ontouchstart",$conteudo);
            echo utf8_encode($conteudo);
          @endphp
        </div>




        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                @php
                  $ultimoCurso = '';
                @endphp
                @foreach ($ConteudoSumario as $dados)
                  @if($ultimoCurso != $dados['Nome_Curso'])
                    {{utf8_encode('<h4>'.$dados['Nome_Curso'].'</h4><hr>')}}
                  @endif
                  @if($Aula[6] > $dados['Ordem'])
                    <a href ="/ameai_mobile/aula/{{$infoAula[5]}}/{{$dados['Ordem']}}/1" onclick="carregandoAula()">{{$dados['Nome_Capitulo']}}</a>
                    <div style="float:right;"><i class="material-icons" style="color:green;">&#xe876;</i></div>
                    <br>
                    <hr>
                  @else
                    <a href ="javascript:void(0)" Onclick="aulaBloqueada()" style="color:#8B8989;">{{utf8_encode($dados['Nome_Capitulo'])}}</a>
                    <div style="float:right"><i class="material-icons" style="color:gray;">&#xe876;</i></div>
                    <br>
                    <hr>
                    @php
                      $ultimoCurso = $dados['Nome_Curso'];
                    @endphp
                  @endif
                @endforeach
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>

      </div>

    </div>
  </div>
  <script src="/assets/js/jquery.easing.min.js"></script>
  <script src="/assets/js/classie.js"></script>
  <script src="/assets/js/gnmenu.js"></script>
  <script src="/assets/js/jquery.scrollTo.js"></script>
  <script src="/assets/js/nivo-lightbox.min.js"></script>
  <script src="/assets/js/stellar.js"></script>
  <!-- Custom Theme JavaScript -->
  <script src="/assets/js/custom.js"></script>
  <script src="/assets/js/contactform.js"></script>


  <script type="text/javascript">

    var aula_3 = @json($Aula[3]);
    var aula_2 = @json($Aula[2]);
    var idioma = @json(utf8_encode($infosCurso[0]['Idioma']));
    var id_curso_info = @json($infosCurso[0]['Id_Curso']);
    var idioma_fala = @json($infosCurso[0]['Idioma_Fala']);

  </script>
  <script src="/js/aula.js"></script>

  </body>

</html>
