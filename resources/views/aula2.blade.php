@extends('base')

@section('section')

  {{-- @component('component/floating-tabs')
  @endcomponent --}}
  <div class="dashboard-main-wrapper">

    @component('component/modal_material_apoio', compact('materialApoio','aula'))
    @endcomponent

    {{-- @component('component/navbar', compact('curso','class'))
    @endcomponent --}}

    {{-- @component('component/left-sidebar')
    @endcomponent --}}

    <div class="dashboard-wrapper">

      <div class="container-fluid dashboard-content">

        {{-- @component('component/content-nav', compact('curso'))
        @endcomponent --}}

        @component('component/content', compact('texto','tab','aluno','curso','class'))
        @endcomponent

      </div>

    </div>

    {{-- <button type="button" class="btn btn-secondary btn_feedback" onclick="getTiposFeedback();">
      <i class="fas fa-info"></i>
    </button> --}}



  </div>

  <script>

    let lastLog = 0;
    let activeTab = @json($tab);

    function salvarAcessoAmeai() {

      var aluno = @json($aluno);
      var curso = @json($curso);
      var class_cap = @json($class);

      let formData = new FormData();

      formData.set("id_usuario", aluno.Id_Usuario);
      formData.set("id_curso", curso.Id_Curso);
      formData.set("id_capitulo", class_cap.id_capitulo);
      formData.set("tab", activeTab);
      formData.set("lastLog", lastLog);

      $.ajax({

        url: '/api/salvarAcessoAmeai',
        data: formData,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function (data, textStatus, jQxhr) {
          if(data) {
            lastLog = data.lastLog;
          }
        },
        error: function (jqXhr, textStatus, errorThrown) {}
      });
    }

    setInterval(salvarAcessoAmeai, 30000);

    function getTiposFeedback() {

      $('#type_feedback').empty();

      $.ajax({
        url: '/aluno/get_feedback_modal',
        type: 'post',
        success: function (data, textStatus, jQxhr) {

          $.each(JSON.parse(data), function(key, value) {
            $('#type_feedback').append("<option value='"+value.id_tipo_feedback+"'>"+value.descricao+"</option>");
          });

          $("#modal_feedback").modal('show');
        },
        error: function (jqXhr, textStatus, errorThrown) {

          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Tivemos um problema para excluir o seu tipo de feedback!'
          });

        }

      });
    }

  </script>

@endsection
