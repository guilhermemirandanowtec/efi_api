<!doctype html>

<html lang="en">

    @component('component/header')
    @endcomponent

    <body>

        @yield('section')

        {{-- @component('component/modal_feedback')
        @endcomponent --}}

        @component('component/scripts', ['nome_aluno' => $aluno->Nome, 'email_aluno' => $aluno->Email])
        @endcomponent

    </body>

</html>
