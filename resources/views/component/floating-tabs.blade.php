<style>
    body {
        font-family: sans-serif;

    }
    .fab {
        position: fixed;
        bottom:10px;
        right:10px;
        z-index:10;
    }
    .fab button {
        cursor: pointer;
        width: 48px;
        height: 48px;
        border-radius: 30px;
        background-color: #00455E;
        color: #FFFFFF;
        border: none;
        box-shadow: 0 1px 5px rgba(0,0,0,.4);
        font-size: 24px;
        -webkit-transition: .2s ease-out;
        -moz-transition: .2s ease-out;
        transition: .2s ease-out;
    }
    .fab button:focus {
        outline: none;
    }
    .fab button.main {
        position: absolute;
        width: 60px;
        height: 60px;
        border-radius: 30px;
        background-color: #00455E;
        color: #FFFFFF;
        right: 0;
        bottom: 0;
        z-index: 20;
    }

    .fab button.listening-button {
        position: absolute;
        width: 60px;
        height: 60px;
        border-radius: 30px;
        background-color: #00455E;
        color: #FFFFFF;
        right: 0;
        bottom: 70px;
        z-index: 20;
    }
    .fab button.main:before {
        transition: all 0.2s ease;
        font-family: 'Font Awesome 5 Free';
        font-weight: 900;
        content: '\f142';
    }
    .fab ul {
        position:absolute;
        bottom: 0;
        right: 0;
        padding:0;
        padding-right:5px;
        margin:0;
        list-style:none;
        z-index:10;
        -webkit-transition: .2s ease-out;
        -moz-transition: .2s ease-out;
        transition: .2s ease-out;
    }
    .fab ul li {
        display: flex;
        justify-content: flex-start;
        position: relative;
        margin-bottom: -10%;
        opacity: 0;
        -webkit-transition: .3s ease-out;
        -moz-transition: .3s ease-out;
        transition: .3s ease-out;

    }
    .fab ul li label {
        margin-right:10px;
        white-space: nowrap;
        display: none;
        margin-top: 10px;
        padding: 5px 8px;
        background-color: white;
        box-shadow: 0 1px 3px rgba(0,0,0,.2);
        border-radius:3px;
        height: 18px;
        font-size: 16px;
        pointer-events: none;
        opacity:0;
        -webkit-transition: .2s ease-out;
        -moz-transition: .2s ease-out;
        transition: .2s ease-out;
    }
    .fab.show button.main,
    .fab.show button.main {
        outline: none;
        background-color: #00455E;
        color: #FFFFFF;
        box-shadow: 0 3px 8px rgba(0,0,0,.5);
    }
    .fab.show button.main:before,
    .fab.show button.main:before {
        font-family: 'Font Awesome 5 Free';
        font-weight: 900;
        content: '\f141';
    }
    .fab.show button.main + ul,
    .fab.show button.main + ul {
        bottom: 70px;
    }
    .fab.show button.main + ul li,
    .fab.show button.main + ul li {
        margin-bottom: 10px;
        opacity: 1;
    }
    .fab.show button.main + ul li:hover label,
    .fab.show button.main + ul li:hover label {
        opacity: 1;
    }
    .hide {
        display:none;
    }
</style>

<div class="fab hide-desktop hide-menu">

  <button class="listening-button hide" onclick="youAreListeningMobileButton(0, 1, true)"> <i class='fas fa-play'></i> </button>
  <button id="btn_menu" class="main"> </button>
  <ul class="d-none" id="btns_menu_lateral">
    <li>

      <label for="tab_dialog">Dialogue </label>
      <button onclick="changeTab(1)" id="tab_dialog"> <i class="fas fa-comments"></i> </button>
    </li>
    <li>
      <label for="opcao2"> Vocabulary </label>
      <button onclick="changeTab(7)" id="opcao2"> <i class="fas fa-book-open"></i> </button>
    </li>
    <li>
      <label for="opcao3"> Grammar </label>
      <button onclick="changeTab(4)" id="opcao3"> <i class="fas fa-spell-check"></i> </button>
    </li>
    <li>
      <label for="opcao3"> Media </label>
      <button onclick="changeTab(6)" id="opcao3"> <i class="fas fa-play"></i> </button>
    </li>
    <li>
      <label for="opcao3"> Write </label>
      <button onclick="changeTab(5)" id="opcao3"> <i class="fas fa-pencil-alt"></i> </button>
    </li>
    <li>
      <label for="opcao3"> More </label>
      <button onclick="changeTab(8)" id="opcao3"> <i class="fas fa-plus"></i></button>
    </li>
    <li>
      <label for="opcao3"> Talk </label>
      <button onclick="changeTab(2)" id="opcao3"> <i class="fas fa-microphone"></i> </button>
    </li>
  </ul>

</div>

<script>
    function toggleFAB(fab) {
        if(document.querySelector(fab).classList.contains('show')) {
            document.querySelector(fab).classList.remove('show');
        } else {
            document.querySelector(fab).classList.add('show');

        }
    }
    document.querySelector('.fab .main').addEventListener('click', function() {
        if (!$('.listening-button').hasClass('hide')) {
            $('.listening-button').addClass('hide')
        }
        toggleFAB('.fab');
    });
    document.querySelectorAll('.fab ul li button').forEach((item)=>{
        item.addEventListener('click', function(){
            toggleFAB('.fab');
        });
    });

</script>
