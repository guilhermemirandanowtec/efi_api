<style>

  .top-navbar-toggler {
    background-color: transparent;
    border: 0px;
    font-size: 30px;
    color: #FFFFFF;
  }

  .top-navbar {

    background-color: rgb(255, 255, 255);
    padding: 20px;
    -webkit-box-shadow: 0 0 10px #003146;
    box-shadow: 0 0 10px #003146;
  }

  .btn-warning {
    background-color: #F7D611;
  }

</style>

<div class="dashboard-header fixed-top">
  <nav class="navbar bg-blue">
    <a class="navbar-brand" href="/">
      <img src="{{asset('/assets/images/logo-branco.png')}}" height="30px">
    </a>
    <div style="width: fit-content; margin: auto">
      <span class="mobile-module-title">
        <i class="fas fa-graduation-cap"></i> {{$curso->Nome_Curso}}
      </span>
      <span class="mobile-module-class">
        <i class="fas fa-file-alt"></i> {{$class[0]->Nome_Capitulo}}
      </span>
      @if(in_array($id_capitulo, $capArray))
        <span class="mobile-module-class" style="margin-left: 10px">
          <button onclick="$('#materialApoioModal').modal()" class="btn btn-warning" style="background-color: transparent; color: #fff;"> Material de Apoio </button>
        </span>
      @endif
      <div class="hide-desktop">
        <nav class="navbar-expand-lg" style="position: fixed; top: 8px; right: 30px; background-color: #01638C;">
          <button class="top-navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
          </button>
        </nav>
      </div>
    </div>
  </nav>
  <div class="collapse navbar-collapse hide-desktop top-navbar" id="navbarNav">
    @component('component/menu-items')
    @endcomponent
  </div>
</div>
