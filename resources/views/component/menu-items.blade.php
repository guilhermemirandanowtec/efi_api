<div class="pull-left image hide-desktop" style="float: left!important;">
  <img src="{{'http://efi-bucket.s3.amazonaws.com/'.$aluno->ImagemUsuario}}" class="user-avatar-lg rounded-circle" alt="Usuario" style="margin-right:5px">
</div>
<ul class="navbar-nav flex-column">
  <li class="nav-item ">
    {{-- @component('component/user_panel')
    @endcomponent --}}
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/aluno">
      <i class="fas fa-home"></i> Página Inicial
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" >
      <i class="fas fa-book"></i> Manual do Aluno
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2">
      <i class="fas fa-tasks"></i> Sumário
    </a>
    <div id="submenu-2" class="collapse submenu" style="margin-bottom: 80px;">
      <ul class="nav flex-column">
        @foreach($sumario as $index => $item)
          <li class="nav-item">
            
          </li>
        @endforeach
      </ul>
    </div>
  </li>
  @if(in_array($id_capitulo, $capArray))
    <li class="nav-item" id="btn_feedback_mobile">
      <a class="nav-link" style="color: #01638c;" data-toggle="modal" data-target="#materialApoioModal" >
        <i class="fas fa-folder-plus"></i> Material de Apoio
      </a>
    </li>
  @endif
  <li class="nav-item" id="btn_feedback_mobile">
    <a class="nav-link" style="color: #01638c;" data-toggle="modal" data-target="#modal_feedback" >
      <i class="fas fa-info"></i> Feedback
    </a>
  </li>
</ul>
