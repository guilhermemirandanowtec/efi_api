<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="{{secure_asset('/assets/libs/js/slimscroll/jquery.slimscroll.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="{{secure_asset('/assets/libs/js/main-js.js')}}"></script>
<script src="{{secure_asset('/assets/libs/js/ameai.js')}}"></script>
<script src="{{secure_asset('/assets/libs/js/ameai/listening.js')}}"></script>
<script src="{{secure_asset('/assets/libs/js/ameai/talk.js')}}"></script>
<script src="{{secure_asset('/assets/libs/js/ameai/write.js')}}"></script>
<script src="{{secure_asset('/assets/libs/js/jmask/jquery.mask.js')}}"></script>
<script src="{{secure_asset('/assets/libs/js/validate/jquery.validate.js')}}"></script>
<script src="{{secure_asset('/assets/libs/js/validate/additional-methods.js')}}"></script>
<script src="{{secure_asset('/assets/libs/js/validate/validate_functions.js')}}"></script>
<script>
    $("#btn_menu").click(function(){
        if ($('.hide-menu').find('ul').hasClass('d-none')) {

            $('.hide-menu').find('ul').removeClass('d-none');

        }

        if ($('.hide-menu').hasClass('show')) {

            $('.hide-menu').find('ul').find('li').removeClass('d-none');


        }else{
           $('.hide-menu').find('ul').find('li').addClass('d-none');
            $('.hide-menu').find('ul').addClass('d-none');

        }

    });
    $(document).ready(function(){
        $('.hide-menu').find('ul').addClass('d-none');

    });
</script>

<!--Start of Tawk.to Script-->
{{-- <script type="text/javascript">

let userName = @json($nome_aluno);
let userEmail = @json($email_aluno);

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5df76e2dd96992700fcc7e26/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();

Tawk_API.visitor = {
    name  : userName,
    email : userEmail
};

</script> --}}
<!--End of Tawk.to Script-->
