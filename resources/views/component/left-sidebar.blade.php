<style>

    .user-panel {

        position: relative;
        width: 100%;
        height: auto;

    }

    .check_done {

        color: #088901!important;
        font-size: 15px!important;

    }

    .check_undone {

        color: #989898!important;
        font-size: 10px!important;

    }

    .check_not_done {

        font-size: 15px!important;

    }

    .toggle-menu:hover {

        box-shadow: none;

    }

    .toggle-menu {

        border-radius: 30px;
        width: 30px;
        height: 30px;
        background-color: #ffffff;
        margin-left: 275px;
        margin-top: 20px;
        position: fixed;
        z-index: 10;
        box-shadow: 2px 2px 6px 0px #CCCCCC;
        -webkit-transition: all 0.3s ease;

    }

    .toggle-menu i {

        margin: 9px;

    }

    @media only screen and (max-width:768px) {

        .toggle-menu {

            display: none;

        }

        .nav-left-sidebar {

            display:none;

        }

        .mobile-module-title {

            display:none;
        }

    }

    @media only screen and (orientation: landscape) and (max-width:800px) {

        .toggle-menu {

            display: none;

        }

    }

</style>

<div class="nav-left-sidebar sidebar-dark" id="ameai-sidebar">

    <div class="toggle-menu toggleOpen" onclick="toggleMenu()">

        <i class="fas fa-bars"></i>

    </div>


    <div class="menu-list">

        <nav class="navbar navbar-expand-lg navbar-light">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <span class="navbar-toggler-icon"></span>

            </button>

            <div class="collapse navbar-collapse hide-desktop" id="navbarNav">

                <?php include 'menu-itens.php'?>

            </div>

        </nav>

    </div>

</div>

<script>


    function toggleMenu() {

        if($('.toggle-menu').hasClass('toggleOpen')) {

            $('.toggle-menu').removeClass('toggleOpen');
            $('.toggle-menu').css('margin-left', '5px');
            $('#ameai-sidebar').width(0);
            $('.dashboard-wrapper').css('margin-left', '13px');
            $('.dashboard-wrapper').addClass('sidebar-closed');

        } else {

            $('.toggle-menu').addClass('toggleOpen');
            $('.toggle-menu').css('margin-left', '275px');
            $('#ameai-sidebar').width('264px');
            $('.dashboard-wrapper').css('margin-left', '288px');
            $('.dashboard-wrapper').removeClass('sidebar-closed');

        }

    }

</script>
