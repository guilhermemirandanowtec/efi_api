<div class="modal fade" id="modal_feedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Dê o seu Feedback</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="type_feedback" style="color: #3d405c!important;">Estou dando um feedback sobre?<span style="color: red;">*</span></label>
                            <select class="form-control" id="type_feedback" style="color:black;" name="type_feedback">

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="about" style="color: #3d405c!important;">Nos diga mais sobre<span style="color: red;">*</span></label>
                            <textarea class="form-control" id="about" name="about" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button onclick='feedbackValidate();' type="button" name="button" id="btn_send_feedback" class="btn btn-primary">Enviar</button>
            </div>
    </div>
  </div>
</div>
