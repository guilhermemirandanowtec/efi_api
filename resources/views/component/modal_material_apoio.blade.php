<style>

  .btn-modal-fechar {
    color: #FFFFFF;
  }

  .close {
    text-shadow: none;
  }

</style>

@php
  $capArray = [];
@endphp

<div class="modal " id="materialApoioModal" tabindex="-1" style="display: none;" role="dialog" aria-labelledby="materialApoioModal" aria-modal="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #01638C;color: white;">
        <h5 class="modal-title modal-title-center d-flex justify-content-center" id="exampleModalLabel">
          <span class="mobile-module-class">
            <i class="fa fa-graduation-cap" style="background-color: #01638C;"></i> Past, Present and Future
          </span>
        </h5>
        <button type="button" class="close btn-modal-fechar" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
    <div class="modal-body">
      <div class="accordion card-shadow" id="accordionExample">
        @foreach ($materialApoio as $key => $material)

          @php
            $cap = $material->Id_Capitulo;
            $nomeCap = $material->Nome_Capitulo;
          @endphp

          @if(!in_array($cap, $capArray))

            <div class="card ">
              <div class="card-header d-flex" id="headingOne">
                <h2 class="mb-0" style="width: 100%;">
                  <button class="btn btn-link btn-action-open" type="button" data-toggle="collapse" data-target="{{'#collapse' . $cap}}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="fa fa-plus"></i> {{$nomeCap}} <i class="icon-action fa fa-chevron-down icon-action-open"></i>
                  </button>
                </h2>
              </div>

              <div id="{{'collapse' . $cap }}" class="collapse {{$cap == $aula ? "show" : "" }}" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                  <div class="container">
                    <div class="row">

                      @foreach ($materialApoio as $key => $materialCol)

                        @if($materialCol->Id_Capitulo == $cap)

                          <div class="col-md-4">
                            <div class="card card-shadow">
                              <div class="card-body d-flex justify-content-center flex-wrap">
                                <div class="row">
                                  <div class="col-md-12 d-flex justify-content-center">
                                    <h5 class="card-title"> {{$materialCol->titulo}} </h5>
                                  </div>
                                  <div class="col-md-12 d-flex justify-content-center">
                                    <i class="fas fa-file-pdf fa-5x"></i>
                                  </div>
                                  <div class="col-md-12 d-flex justify-content-center">
                                    <a href="{{ 'https://efi-bucket.s3-us-west-2.amazonaws.com/' . $materialCol->arquivo}}" class="btn btn-primary btn-download-modal" style="margin-top:18px">
                                      <i class="fa fa-file" style="padding: 0 5px;"></i> Download
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        @endif
                      @endforeach

                    </div>
                  </div>
                </div>
              </div>
            </div>

            {{array_push($capArray,$cap)}}

            @endif

          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
