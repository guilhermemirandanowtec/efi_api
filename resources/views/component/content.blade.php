<style>

  /* #btnFalar{
    display:none;
  } */

  input[type=text] {
    border: 0px solid #696969;
    border-bottom: 2px solid #696969;
    background-color: transparent;
  }

  .btn-ameai {
    background-color: #FFFFFF;
    min-width:130px;
    color: #333333;
  }

  .btn-ameai:hover {
    background-color: #01638C;
    min-width:130px;
    color: #FFFFFF;
  }

  .scroll::-webkit-scrollbar {
    display: none;
  }

  .scroll {
    width: fit-content;
    margin:auto;
  }

  .activeTab {
    background-color: #F7D611;
  }

  .correct-answear {
    border-bottom: 2px solid green!important;
    color: green!important;
    background-color: transparent!important;
    font-weight: bold;
  }

  .wrong-answear {
    border-bottom: 2px solid red!important;
    color: red!important;
    background-color: transparent!important;
    font-weight: bold;
  }

  .writeGrade {
    font-size: 28px;
    font-weight: bold;
  }


  @media only screen and (max-width:1023px) {
    .scroll {

      display:none;

    }
  }

</style>
<style>

input{
  border-bottom: .5px solid rgb(160,160,160) !important;
  border: none;
}

.content-text {
  background-color: #FFFFFF;
  border-radius: 10px;
  margin-top: 10px;
  padding: 20px;
}

.listening_button {
  width:50px;
  height:50px;
  background-color: #01638C;
  color: #FFFFFF;
  border:0;
}

.swal-wide{
  width:850px;
}

@media (min-width: 576px) {
  .modal-dialog {

    max-width: 90%;
    margin: 1.75rem auto;

  }
}

</style>

<div id="listening_button_container" style="width:50px;"></div>

<div class="loading-ameai">
  <img src="https://efi-bucket.s3.amazonaws.com/LoadEFI.gif">
</div>

<input type="hidden" id="id_texto" value="{{$texto->id_texto}}">

<div class="content-text">
  {!!$texto->texto!!}
</div>

<script>
  function refazer_exercicio() {

    let html_refazer = localStorage.getItem('refazerExercicio');
    html_refazer = html_refazer.replace("form_escreva()","form_escreva(true)")

    Swal.fire({
      html: html_refazer,
      customClass: 'swal-wide',
      showCloseButton: true,
      showConfirmButton: false
    });

  }

  $(document).ready(function(){
    changeTab(@json($tab));
  });

</script>

<script>
var aluno = @json($aluno);
var curso = @json($curso);
var class_cap = @json($class);
id_usuario = aluno.Id_Usuario;
capitulo = class_cap.ordem;
id_curso = curso.Id_Curso;
function changeTab(tab) {
  activeTab = tab;
  lastLog = 0;
  $('.hide-menu').find('ul').addClass('d-none');
  id_curso = id_curso;
  capitulo = capitulo;
  $('.loading-ameai').show();
  $('.content-text').html('1Tivemos um problema para carregar o conteudo, por favor, atualize a página e tente novamente');
  $.ajax({

    url: '/api/getTabClassText',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({
      "id_curso": id_curso,
      "capitulo": capitulo,
      "tab": tab,
      "id_aluno": id_usuario
    }),
    processData: false,
    success: function( data, textStatus, jQxhr ) {

      localStorage.setItem('id_texto', data['Id_texto']);
      localStorage.setItem('refazerExercicio', data['exercicio']);

      $('.content-text').html(data['texto']);
      $("#formulario_escreva").attr('autocomplete', 'off');

      mobileSingleColumn();

      if(tab == 2) {

        disableInputText();

      }

      $('.activeTab').removeClass('activeTab')
      idTab = '#' + 'tab' + tab;
      $(idTab).addClass('activeTab');

      if(tab == 5) {

        getWriteAnswear(data['Id_texto']);

      } else {

        $('.loading-ameai').hide();

      }

    },
    error: function( jqXhr, textStatus, errorThrown ){
      console.log( errorThrown );
      $('.loading-ameai').hide();
    }

  });

}

function get_respostas(id_capitulo) {
  respostas = [];
  if(verificaDiferenca()) {
    $('.content-text input[type="radio"]:checked').each(function(index, value){
       respostas.push({
        id: $(this).attr('id'),
        resposta: $(this).val()
        });
    });

    Swal.fire({
    title: 'Tem certeza?',
    text: "Deseja enviar suas respostas!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#01638C',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Enviar',
    cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
        enviarRespostas(respostas, id_capitulo);
        }
    })
  }
  else {
      Swal.fire({
      type: 'warning',
      title: 'Atenção',
      text: 'Responda todas as questões!',
    });
  }
}

function verificaDiferenca() {
  qtdQuestao = $('.content-text input[type="radio"]').length/5;
  qtdOption = $('.content-text input[type="radio"]:checked').length;

  if(qtdQuestao == qtdOption) return true;
  return false;
}

function enviarRespostas(respostas, id_capitulo) {

  respostas =  JSON.stringify(respostas);
  tab = 10;
  let formData = new FormData();
  formData.set("respostas", respostas);
  formData.set("id_capitulo", id_capitulo);
  formData.set("id_curso", id_curso);
  formData.set("id_aluno", id_usuario);
  $('.loading-ameai').show();
  $('.content-text').html('Tivemos um problema para carregar o conteudo, por favor, atualize a página e tente novamente');
  $.ajax({
      url: '/api/salva_tentativa',
      type: 'POST',
      contentType: false,
      processData: false,
      data: formData,
  }).done(function (resposta) {
      $('.activeTab').removeClass('activeTab')
      idTab = '#' + 'tab' + tab;
      $(idTab).addClass('activeTab');
      $('.loading-ameai').hide();
      $('.content-text').html(resposta);

  }).fail(function (jqXHR, textStatus) {
  console.log("Request failed: " + textStatus);

  });
}

function refazer(id_capitulo) {
    getExerciciosExtra(id_capitulo);
}

function getExerciciosExtra (id_capitulo){
  tab = 10;
  let formData = new FormData();
  formData.set("id_capitulo", id_capitulo);
  $('.loading-ameai').show();
  $('.content-text').html('Tivemos um problema para carregar o conteudo, por favor, atualize a página e tente novamente');
  $.ajax({
  url: '/api/buscar_exercicios',
  type: 'POST',
  contentType: false,
  processData: false,
  data: formData,
  }).done(function (resposta) {
      $('.activeTab').removeClass('activeTab')
      idTab = '#' + 'tab' + tab;
      $(idTab).addClass('activeTab');
      $('.loading-ameai').hide();
      $('.content-text').html(resposta);

  }).fail(function (jqXHR, textStatus) {
  console.log("Request failed: " + textStatus);

  });
}

</script>
