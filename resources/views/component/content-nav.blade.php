<style>

  input[type=text] {
    border: 0px solid #696969;
    border-bottom: 2px solid #696969;
    background-color: transparent;
  }

  .btn-ameai {
    background-color: #FFFFFF;
    min-width:130px;
    color: #333333;
  }

  .btn-ameai:hover {
    background-color: #01638C;
    min-width:130px;
    color: #FFFFFF;
  }

  .scroll::-webkit-scrollbar {
    display: none;
  }

  .scroll {
    width: fit-content;
    margin:auto;
  }

  .activeTab {
    background-color: #F7D611;
  }

  .correct-answear {
    border-bottom: 2px solid green!important;
    color: green!important;
    background-color: transparent!important;
    font-weight: bold;
  }

  .wrong-answear {
    border-bottom: 2px solid red!important;
    color: red!important;
    background-color: transparent!important;
    font-weight: bold;
  }

  .writeGrade {
    font-size: 28px;
    font-weight: bold;
  }


  @media only screen and (max-width:1023px) {
    .scroll {

      display:none;

    }
  }

</style>

<div class="container hide-desktop">

  <div class="row">

    <div class="col-1">

      <i class="fas fa-graduation-cap align-bottom"></i>

    </div>

    <div class="col-8">

      <b><?= $curso['Nome_Curso'] ?></b>
      <br>
      <?= $class[0]['Nome_Capitulo'] ?>

    </div>

  </div>

</div>

<div class="row hide-mobile">

  <div class="col-md-12">

    <nav class="scroll">

      <div id="tab1" onclick="changeTab(1)" class="btn btn-ameai btn-lg activeTab" role="button">
        <i class="fas fa-comments"></i>&nbsp; Dialogue
      </div>

      <div id="tab7" onclick="changeTab(7)" class="btn btn-ameai btn-lg" role="button">
        <i class="fas fa-book-open"></i>&nbsp; Vocabulary
      </div>

      <div id="tab4" onclick="changeTab(4)" class="btn btn-ameai btn-lg" role="button">
        <i class="fas fa-spell-check"></i>&nbsp; Grammar
      </div>

      <div id="tab6" onclick="changeTab(6)" class="btn btn-ameai btn-lg" role="button">
        <i class="fas fa-play"></i>&nbsp; Media
      </div>

      <div id="tab5" onclick="changeTab(5)" class="btn btn-ameai btn-lg" role="button">
        <i class="fas fa-pencil-alt"></i>&nbsp; Write
      </div>

      <div id="tab8" onclick="changeTab(8)" class="btn btn-ameai btn-lg" role="button">
        <i class="fas fa-plus"></i>&nbsp; More
      </div>

      <div id="tab2" onclick="changeTab(2)" class="btn btn-ameai btn-lg" role="button">
        <i class="fas fa-microphone"></i>&nbsp; Talk
      </div>

    </nav>

  </div>

</div>

<script>


id_usuario = '<?php
if(!isset($_SESSION['logado_fadic'])) {

  echo $_SESSION['aluno_id'];

} else {
  echo '0';
}
?>';

BASE_URL = '<?php echo BASE_URL; ?>';
capitulo = '<?php echo $capitulo; ?>';
id_curso = '<?php echo $id_curso; ?>'

function changeTab(tab) {

  activeTab = tab;
  lastLog = 0;

  $('.hide-menu').find('ul').addClass('d-none');
  id_curso = '<?php echo $id_curso; ?>';
  capitulo = '<?php echo $capitulo; ?>';

  $('.loading-ameai').show();
  $('.content-text').html('Tivemos um problema para carregar o conteudo, por favor, atualize a página e tente novamente');
  $.ajax({

    url: BASE_URL + '/ameai/getTabClassText/',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({
      "id_curso": id_curso,
      "capitulo": capitulo,
      "tab": tab
    }),
    processData: false,
    success: function( data, textStatus, jQxhr ) {

      localStorage.setItem('id_texto', data['Id_texto']);
      localStorage.setItem('refazerExercicio', data['exercicio']);

      $('.content-text').html(data['texto']);
      $("#formulario_escreva").attr('autocomplete', 'off');

      mobileSingleColumn();

      if(tab == 2) {

        disableInputText();

      }

      $('.activeTab').removeClass('activeTab')
      idTab = '#' + 'tab' + tab;
      $(idTab).addClass('activeTab');

      if(tab == 5) {

        getWriteAnswear(data['Id_texto']);

      } else {

        $('.loading-ameai').hide();

      }

    },
    error: function( jqXhr, textStatus, errorThrown ){
      console.log( errorThrown );
      $('.loading-ameai').hide();
    }

  });

}

</script>
